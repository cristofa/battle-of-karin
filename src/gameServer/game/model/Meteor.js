import Phaser from 'phaser';

const SCALE_FACTOR = 0.15;
const MASS_FACTOR = 5;
const VELOCITY_FACTOR = 250;

class Meteor extends Phaser.Physics.Arcade.Image {

  _id;
  _fraction = 5;

  constructor(scene, x, y, id) {
    super(scene, x, y, 'img', 0);
    this._scene = scene;
    this._id = id;

    scene.physics.world.enable(this);
    this.setCollideWorldBounds(true);
    this.setOrigin(0.5, 0.5).setDisplaySize(100, 100);
    this.setAngle(45);
    this.setDamping(true);
    this.setDrag(0);
    this.setBounce(1, 1);
    this.setMaxVelocity(VELOCITY_FACTOR/this._fraction);
    this.setMass(this._fraction*MASS_FACTOR);
    this.setVelocity(-50 * Math.random()*100, -50+Math.random()*100);
    this.setScale(SCALE_FACTOR*this.fraction);
    scene.add.existing(this);
  }

  getInfo() {
    return {
      id: this._id,
      x: this.x,
      y: this.y,
      fraction: this.fraction,
    };
  }

  hit() {
    this.fraction--;
  }

  reset(newX, newY) {
    this.fraction = 1;
    this.x = newX;
    this.y = newY;
  }

  get fraction() {
    return this._fraction;
  }

  set fraction(value) {
    this._fraction = value;
    if (value > 0) {
      this.setScale(SCALE_FACTOR * value);
      this.setMass(value * MASS_FACTOR);
      this.setMaxVelocity(VELOCITY_FACTOR / this._fraction);
    }
  }
}

export default Meteor;
