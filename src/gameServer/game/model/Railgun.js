import Phaser from 'phaser';

import WeaponType from './BulletType';

class Railgun extends Phaser.Physics.Arcade.Sprite {

  armed = false;
  constructor(scene, parent, x, y) {
    super(scene, x, y, 'bullet');
    this._scene = scene;
    this.parent = parent;
    scene.physics.world.enable(this);
    this.setOrigin(0.5, 0.5);
    this.setScale(0.3, 0.3);
    this.setImmovable(true);
    this.body.onOverlap = true;
    scene.add.existing(this);

  }

  hit() {
  }

  fire(parent, players, playerHitCallback, meteors, meteorHitCallback) {

    const activePlayers = players.filter(p => p.life > 0 && !p.protection);
    activePlayers.forEach(p => {
      const angle = parent.angle - Math.atan2(p.y - parent.y, p.x - parent.x) * Phaser.Math.RAD_TO_DEG;
      const targetDistance = Phaser.Math.Distance.BetweenPoints(parent, p);
      if (targetDistance < 600) {
        if (Math.abs(angle) < Math.min(1800 / targetDistance, 30)) {
          playerHitCallback(p, this);
        }
      }
    });
    meteors.forEach(m => {
      const angle = parent.angle - Math.atan2(m.y - parent.y, m.x - parent.x) * Phaser.Math.RAD_TO_DEG;
      const targetDistance = Phaser.Math.Distance.BetweenPoints(parent, m);
      if (targetDistance < 600) {
        if (Math.abs(angle) < Math.min((400*m.fraction) / targetDistance, 30)) {
          meteorHitCallback(m, this);
        }
      }
    });
  }

  getType() {
    return WeaponType.RAILGUN;
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);
    this.time++;
    if (this.time > 8) {
      this.armed = false;
      this.setActive(false);
      this.disableBody(true, true);
    }
  }

}

export default Railgun;
