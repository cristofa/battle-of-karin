import Phaser from 'phaser';

import Player from './Player';

const UPDATE_EVERY = 90;

const FOV_RADIUS = 600;
const FIRE_FOV = 20;
const RAILGUN_FOV = 7;

const WORLD_OFFSET = 200;

const mode = {
  SEARCH: 'search',
  ATTACK: 'attack'
};

class Bot extends Player {

  _input = [{right: false, left: true, up: false, down: false, space: false, xKey: false, cKey: false}];
  _lastUpdate = 0;

  constructor(scene, x, y, bulletGroup, railgunBulletsGroup) {
    super(scene, x, y, bulletGroup);
    this._scene = scene;
    this._bot = true;
    this._mode = mode.SEARCH;
    this._target = null;
    this._graphics = scene.add.graphics({ lineStyle: { width: 2, color: 0xaaaa00 } });
    if (railgunBulletsGroup) {
      railgunBulletsGroup.add(this.railgun);
    }
  }

  fixedAngle(a) {
    a += a > 180 ? -360 : a < -180 ? 360 : 0;
    return a;
  }

  updateState(time, players) {
    let input = {right: false, left: true, up: false, down: false, space: false, xKey: false, cKey: false};
    input.space = false;

    if (time - this._lastUpdate > UPDATE_EVERY) {
      this._fov = new Phaser.Geom.Circle(this.x, this.y, FOV_RADIUS);


      const playerPoints = players.filter(p => (p.x !== this.x && p.y !== this.y) &&
        p.life > 0 && !p.protection && Phaser.Geom.Circle.ContainsPoint(this._fov, p));

      const angles = playerPoints
        .map(p => this.fixedAngle(this.angle - Math.atan2(p.y - this.y, p.x - this.x) * Phaser.Math.RAD_TO_DEG));

      const inFront = angles.filter(a => Math.abs(a) < FIRE_FOV);
      const rightInFront = angles.filter(a => Math.abs(a) < RAILGUN_FOV);

      switch (this._mode) {

      case mode.SEARCH:
        if (angles.length > 0) {
          this._mode = mode.ATTACK;
          this._target = playerPoints[0];
        }
        break;
      case mode.ATTACK:
        if (angles.length == 0 || this._target.life == 0 || !this._target.body) {
          this._target = null;
          this._mode = mode.SEARCH;
        }
        break;
      }


      if (inFront.length  > 0) {
        input.space = Math.random() > 0.5;
      }
      const absAngle = Math.abs(this.angle);
      if (this._mode == mode.SEARCH) {
        if (!this._target) {
          const bounds = this._scene.physics.world.bounds;
          if ((this.x < WORLD_OFFSET && absAngle > 90) || (this.x > bounds.width - WORLD_OFFSET && absAngle < 90) ||
            (this.y < WORLD_OFFSET && this.angle < 0) || (this. y > bounds.height - WORLD_OFFSET && this.angle > 0)) {
            this._target = new Phaser.Geom.Point(bounds.width/2, bounds.height/2);
            const direction = this.fixedAngle(this.angle - Math.atan2(this._target.y - this.y, this._target.x - this.x) * Phaser.Math.RAD_TO_DEG);
            this._turnDirection = direction;
          }
        } else  {
          const targetDirection = this.fixedAngle(this.angle - Math.atan2(this._target.y - this.y, this._target.x - this.x) * Phaser.Math.RAD_TO_DEG);
          if (Math.abs(targetDirection) < FIRE_FOV) {
            this._target = null;
          }
        }

        if (this._target !== null) {
          input.up = false;
          input.down = true;
          input.left = false;
          input.right = false;

          if (this._turnDirection > 0) {
            input.left = true;
          } else {
            input.right = true;
          }
        } else {
          input.left = false;
          input.right = false;
          input.up = true;
          if (this._powered) {
            input.xKey = true;
          } else {
            input.xKey = false;
          }
          input.down = false;
        }

      } else if (this._mode == mode.ATTACK) {
        const targetVelocity = this._target.body.velocity;
        const targetNextPosition = new Phaser.Geom.Point(this._target.x + targetVelocity.x/3, this._target.y + targetVelocity.y/3);
        this._turnDirection = this.fixedAngle(this.angle - Math.atan2(targetNextPosition.y - this.y, targetNextPosition.x - this.x) * Phaser.Math.RAD_TO_DEG);
        input.left = false;
        input.right = false;

        if (Math.abs(this._turnDirection) > 5) {
          if (this._turnDirection > 0) {
            input.left = true;
          } else {
            input.right = true;
          }
        }
        const targetDistance = Phaser.Math.Distance.BetweenPoints(this, this._target);
        input.up = targetDistance > WORLD_OFFSET;
        input.xKey = targetDistance > (WORLD_OFFSET*2);
        input.down = targetDistance < 100;
        if (rightInFront.length > 0 && targetDistance < 500) {
          input.cKey = true;
        }
      }
      this._input.push(input);
      this._lastUpdate = time;
      return true;
    }

    return false;
  }


  get input() {
    return this._input;
  }

}

export default Bot;
