import Phaser from 'phaser';

import Bullet from './Bullet';

class Gun extends Phaser.Physics.Arcade.Group {
  _group;
  _count;
  _parent;

  constructor(scene, parent, group) {
    super(scene.physics.world, scene, {
      key: 'bullet',
      active: true,
      visible: true,
      classType: Bullet
    });
    this._count = 0;
    this._parent = parent;
    this._group = group;
    this.clear(true, true);
  }

  fireBullet(x, y, velocity) {
    let bullet = this.getFirstDead(this._count < 5);
    if (bullet) {
      if (typeof bullet.id == 'undefined') {
        this._group.add(bullet);
        bullet.parent = this._parent;
        bullet.id = this._count++;
      }
      bullet.fire(x, y, velocity);
    }
  }


}

export default Gun;
