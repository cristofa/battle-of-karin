import Phaser from 'phaser';

import WeaponType from './BulletType';

class Bullet extends Phaser.Physics.Arcade.Sprite {

  armed = false;
  constructor(scene, x, y) {
    super(scene, x, y, 'bullet');
    this.setScale(1);
  }

  fire(x, y, velocity) {
    this.time = 0;
    this.armed = true;
    this.enableBody(true, x, y, true, true);
    this.setActive(true);
    this.setVelocityX(velocity.x);
    this.setVelocityY(velocity.y);
  }

  getType() {
    return WeaponType.BULLET;
  }

  hit() {
    if (this.armed) {
      this.armed = false;
      this.time -= 20;
    }
  }

  preUpdate(time, delta) {
    super.preUpdate(time, delta);
    this.time++;
    if (this.time > 70) {
      this.armed = false;
      this.setActive(false);
      this.disableBody(true, true);
    }
  }

}

export default Bullet;
