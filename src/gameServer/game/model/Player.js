import Phaser from 'phaser';

import Gun from './Gun';
import Railgun from './Railgun';

class Player extends Phaser.Physics.Arcade.Image {

  _bot = false;
  _points = 0;
  _input = [];
  _life = 3;
  _lastActiveTime;
  _timeOfDead;
  _timeOfPowerUse;
  _protection = true;
  _powered = true;
  _streak = 0;
  _sentVictimMessage = false;

  constructor(scene, x, y, bulletsGroup, railgunBulletsGroup) {
    super(scene, x, y, 'img', 0);
    this._scene = scene;
    scene.physics.world.enable(this);
    this.setCollideWorldBounds(true);
    this.setOrigin(0.5, 0.5).setDisplaySize(53, 40);
    this.setAngle(0);
    this.setBounce(0.5, 0.5);
    this.setDamping(true);
    this.setDrag(0.9);
    this.setMaxVelocity(200);

    this.setScale(0.25);
    this.gun = new Gun(scene, this, bulletsGroup);
    this.railgun = new Railgun(scene, this, this.x, this.y);
    if (railgunBulletsGroup) {
      railgunBulletsGroup.add(this.railgun);
    }
    scene.add.existing(this);
  }

  fireBullet() {
    const velocity = this._scene.physics.velocityFromRotation(this.rotation, 500);
    velocity.add(this.body.velocity);
    this.gun.fireBullet(this.x, this.y, velocity);
  }

  fireRailgun(players, playerHitCallback, meteors, meteorHitCallback) {
    console.log('Railgun fired');
    this.railgun.fire(this, players, playerHitCallback, meteors, meteorHitCallback);
  }

  addPoint() {
    this._points++;
  }

  getBullets() {
    return this.gun.getChildren().map(b => {return {id: b.id, x: b.x, y: b.y, active: b.active, armed: b.armed}; });
  }

  getInfo() {
    return {
      x: this.x,
      y: this.y,
      bullets: this.getBullets() || [],
      rotation: this.rotation,
      engines: this.engines,
      points: this.points,
      life: this._life,
      protection: this._protection,
      powered: this._powered
    };
  }

  damage() {
    this._life--;
  }

  reset(newX, newY) {
    this.life = 3;
    this.timeOfDead = null;
    this.protection = true;
    if (newX) {
      this.x = newX;
    }
    if (newY) {
      this.y = newY;
    }
    this.streak = 0;
  }

  set timeOfDead(value) {
    this._timeOfDead = value;
  }

  get life() {
    return this._life;
  }

  set life(value) {
    this._life = value;
  }

  get timeOfDead() {
    return this._timeOfDead;
  }

  get timeOfPowerUse() {
    return this._timeOfPowerUse;
  }

  set timeOfPowerUse(value) {
    this._timeOfPowerUse = value;
  }

  get lastActiveTime() {
    return this._lastActiveTime;
  }

  set lastActiveTime(value) {
    this._lastActiveTime = value;
  }

  get bullets() {
    return this.gun;
  }

  get input() {
    return this._input;
  }

  set input(value) {
    this._input = value;
  }

  get bot() {
    return this._bot;
  }

  get points() {
    return this._points;
  }

  set points(value) {
    this._points = value;
  }

  get protection() {
    return this._protection;
  }

  set protection(value) {
    this._protection = value;
  }

  get streak() {
    return this._streak;
  }

  set streak(value) {
    this._streak = value;
  }

  get powered() {
    return this._powered;
  }

  set powered(value) {
    this._powered = value;
  }

  get sentVictimMessage() {
    return this._sentVictimMessage;
  }

  set sentVictimMessage(value) {
    this._sentVictimMessage = value;
  }
}

export default Player;
