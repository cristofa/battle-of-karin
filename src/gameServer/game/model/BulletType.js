const WeaponType = {
  BULLET: 0,
  RAILGUN: 1
};

export default WeaponType;
