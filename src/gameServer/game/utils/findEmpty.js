import Phaser from 'phaser';

import commonConfig from '../../../common/commonConfig';

const findEmpty = (players, meteors) => {
  const lookUpCircle = new Phaser.Geom.Circle(400, 300, 100);
  let foundEmpty = false;
  let tries = 0;
  do {
    tries++;
    lookUpCircle.x = Math.random()*commonConfig.game.size.width;
    lookUpCircle.y = Math.random()*commonConfig.game.size.height;
    foundEmpty = true;
    players.getChildren().forEach((player) => {
      if (lookUpCircle.contains(player.x, player.y)) {
        foundEmpty = false;
      }
    });
    meteors.getChildren().forEach((meteor) => {
      if (lookUpCircle.contains(meteor.x, meteor.y)) {
        foundEmpty = false;
      }
    });
  } while(!foundEmpty && tries < 10);
  return {newX: lookUpCircle.x, newY: lookUpCircle.y};
};

export default findEmpty;
