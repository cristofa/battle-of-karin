
const checkBulletsArmed = (bullet1, bullet2) => {
  return bullet1.armed && bullet2.armed && (bullet1.parent.playerId != bullet2.parent.playerId);
};

const checkBulletArmed = (meteor, bullet) => {
  return bullet.armed;
};

const checkBulletAndPlayerAlive = (player, bullet) => {
  return bullet.armed && player.life > 0 && !player.protection && bullet.parent.playerId != player.playerId;
};

const checkPlayersAlive = (player1, player2) => {
  return player1.life > 0 && player2.life > 0 && !player1.protection && !player2.protection;
};

const checkPlayerAlive = (player) => {
  return player.life > 0 && !player.protection;
};

const bulletsCollide = (bullet1, bullet2) => {
  bullet1.hit();
  bullet2.hit();
};

export {
  checkBulletAndPlayerAlive,
  checkBulletsArmed,
  checkPlayersAlive,
  checkPlayerAlive,
  bulletsCollide,
  checkBulletArmed
};
