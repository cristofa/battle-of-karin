import commonConfig from '../../../common/commonConfig';

const getBackendURI = () => {
  let backendPort = process.env.NODE_ENV === 'development' ? `:${commonConfig.port}` : '';
  return `http://${commonConfig.host}${backendPort}`;
};

export default getBackendURI;
