import msgpack from 'msgpack-lite';

import NET_MSGS from '../../../common/NET_MSGS';
import consoleMsg from '../../../common/consoleMsg';

class ServerSocket {

  // [ rotation, points, 5x(x, y, armed, active)
  defaultInfo = {
    rotation: 0,
    points: 0,
    bullets: [],
    input: [{
      left: false,
      right: false,
      up: false,
      down: false,
      space: false,
      xKey: false,
      cKey: false
    }]
  };

  constructor(namespace, worldWidth, worldHeight) {
    this.players = {};
    this.sockets = {};
    this.meteors = {};
    this._id = 0;
    this._namespace = namespace;
    this._worldWidth = worldWidth;
    this._worldHeight = worldHeight;
    // eslint-disable-next-line no-undef
    this.socket = io;
    //this.socket = null;
  }

  getId() {
    const id =this._id++;
    if (this._id > Number.MAX_SAFE_INTEGER) {
      this._id = 0;
    }
    return id;
  }

  getPlayersUpdate(info) {
    const arr = [info.id,info.x, info.y, info.rotation, info.protection, info.engines, info.life, info.powered];
    info.bullets.filter(b => b.active).forEach(b => arr.push(...[b.id, b.x, b.y, b.active, b.armed]));
    return arr;
  }

  getMeteorUpdate(info) {
    const arr = [info.id,info.x, info.y, info.fraction];
    return arr;
  }

  sendUpdate() {
    if (!this.socket) {
      return;
    }
    const update = [];
    Object.keys(this.players).forEach(p => {
      update.push(...this.getPlayersUpdate(this.players[p]));
      update.push(Number.MIN_SAFE_INTEGER);
    });
    update.push(Number.MAX_SAFE_INTEGER);
    Object.keys(this.meteors).forEach(m => {
      update.push(...this.getMeteorUpdate(this.meteors[m]));
    });
    update.push(Number.MAX_SAFE_INTEGER);
    this.socket.of(this._namespace).compress(false).emit(NET_MSGS.PLAYER.UPDATE, msgpack.encode(update));
  }
  sendTime(time) {
    this.socket.of(this._namespace).emit(NET_MSGS.TIME, time);
  }

  sendRanking() {
    const ranking = Object.values(this.players).map(p => { return {
      points: p.points,
      name: p.playerName,
      rank: p.rank,
      id: p.playerId
    };});
    this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.RANKING, ranking);
  }

  sendWinners() {
    let maxPoints = 0;
    Object.values(this.players).forEach(p => {
      if (p.points > maxPoints) {
        maxPoints = p.points;
      }
    });

    const winners = Object.values(this.players).filter(p => p.points == maxPoints && p.points > 0).map(p => p.playerId);
    this.socket.of(this._namespace).emit(NET_MSGS.WINNERS, winners);
  }

  sendKillInfo(victim, killer, firstBlood, weaponType) {
    if (!this.socket) {
      return;
    }
    const msg = {
      victim: victim.playerId,
      killer: killer.playerId,
      killerStreak: killer.streak,
      killerPoints: killer.points,
      killerName: killer.playerName,
      victimStreak: victim.streak,
      victimName: victim.playerName,
      weapon: weaponType
    };
    if (!firstBlood) {
      msg.firstBlood = true;
    }
    this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.KILL, msg);
  }

  sendAccelAction(id) {
    if (!this.socket) {
      return;
    }
    const msg = this.players[id].id;

    this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.ACCELERATE, msg);
  }

  sendRailgunShot(player) {
    if (!this.socket) {
      return;
    }
    const msg = [this.players[player.playerId].id, player.x, player.y, player.rotation];

    this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.RAILGUN_SHOT, msg);
  }

  removeBot(id) {
    this.socket.of(this._namespace).emit(NET_MSGS.DISCONNECT, id);
    delete this.players[id];
  }

  initBotInfo(id, x, y) {
    const info = {...this.defaultInfo};
    info.id = this.getId();
    info.playerId = id;
    info.playerName = id;
    info.x = x;
    info.y = y;
    this.players[id] = info;
    this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.NEW, this.players[id]);
    this.sendRanking();
  }

  clearMeteors() {
    this.meteors = {};
  }

  setMeteorInfo(info) {
    this.meteors[info.id] = info;
  }
  removeMeteor(info) {
    delete this.meteors[info.id];
  }

  initMeteorInfo(info) {
    this.meteors[info.id] = info;
  }

  sendBotMsgToKiller(killerId, botId, msgId) {
    if (!this.socket) {
      return;
    }
    this.sockets[killerId].emit(NET_MSGS.PLAYER.MSG_FROM_VICTIM, {victimName: this.players[botId].playerName, msgId});
  }

  sendConsoleMsg(msgType, params) {
    if (!this.socket) {
      return;
    }
    this.socket.of(this._namespace).emit(NET_MSGS.CONSOLE, {type: msgType, params});
  }

  sendPlayerTo(id, namespace) {
    this.sockets[id].emit(NET_MSGS.PLAYER.CHANGE_GAME, {namespace});

  }
  setPlayerInfo(id, info) {
    Object.assign(this.players[id], info);
  }

  kickPlayer(id) {
    this.sockets[id].emit(NET_MSGS.KICKED_OUT);
    this.sockets[id].disconnect(0);
  }

  init(handlers) {
    if (!this.socket) {
      return;
    }
    const players = this.players;
    const ioSocket = this.socket;
    // eslint-disable-next-line no-undef
    ioSocket.of(this._namespace).on('connection', (socket) => {

      socket.on('disconnect', () => {
        handlers[NET_MSGS.DISCONNECT](socket.id);
        if (!this.players[socket.id]) {
          return;
        }

        this.socket.of(this._namespace).emit(NET_MSGS.CONSOLE, {type: consoleMsg.LEFT,
          params: {playerName: this.players[socket.id].playerName, rank: this.players[socket.id].rank}});
        console.log(`${this.players[socket.id].playerName} disconnected`);

        delete this.players[socket.id];
        this.socket.of(this._namespace).emit(NET_MSGS.DISCONNECT, socket.id);
      });

      socket.on(NET_MSGS.LOGIN, async (payload) => {
        let playersData = {};
        if (payload.playerId) {
          const details = await window.getPlayersData(payload.playerId).catch(() => {
            console.log('Failed to get player details.');
          });
          if (typeof details == 'undefined') {
            socket.emit(NET_MSGS.ERROR);
            socket.disconnect(0);
            return;
          }
          playersData.playerName = details.userName;
          playersData.bodyColor = details.bodyColor;
          playersData.cockpitColor = details.cockpitColor ;
          playersData.flameColor = details.flameColor;
          playersData.rank = details.rank;
        } else {
          playersData.playerName = `Guest${Math.floor(Math.random()*1000)}`;
          playersData.rank = -2;
        }
        console.log(`player ${playersData.playerName} joined ${this._namespace}`);
        this.socket.of(this._namespace).emit(NET_MSGS.CONSOLE,
          {type: consoleMsg.JOINED, params: {playerName: playersData.playerName, rank: playersData.rank}});
        socket.emit(NET_MSGS.PLAYER.INIT, players);

        const info = {...this.defaultInfo};
        info.playerId = socket.id;
        info.id = this.getId();
        info.playerName = playersData.playerName;
        info.rank = playersData.rank;
        if (playersData.bodyColor) {
          info.bodyColor = playersData.bodyColor;
          info.cockpitColor = playersData.cockpitColor;
          info.flameColor = playersData.flameColor;
        }
        info.x = Math.random() * this._worldWidth;
        info.y = Math.random() * this._worldHeight;
        info.life = 3;
        info.protection = true;
        players[socket.id] = info;
        this.sockets[socket.id] = socket;
        handlers[NET_MSGS.PLAYER.NEW](players[socket.id], payload.playerId);

        this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.NEW, players[socket.id]);
        this.sendRanking();
      });

      socket.on(NET_MSGS.PLAYER.INPUT, (inputData) => {
        handlers[NET_MSGS.PLAYER.INPUT](socket.id, inputData);
      });

      socket.on(NET_MSGS.PLAYER.WARMUP_MSG, (msg) => {
        this.socket.of(this._namespace).emit(NET_MSGS.PLAYER.MSG_FROM_VICTIM, {victimName: msg.sender, msgId: msg.id, warmup: true});
      });

      socket.on(NET_MSGS.PLAYER.MSG_TO_KILLER, (msg) => {
        handlers[NET_MSGS.PLAYER.MSG_TO_KILLER](socket.id);
        if (this.sockets[msg.killerId]) {
          this.sockets[msg.killerId].emit(NET_MSGS.PLAYER.MSG_FROM_VICTIM, {victimName: msg.victimName, msgId: msg.id, warmup: false});
        }
      });
    });
  }
}


export default ServerSocket;
