import Phaser from 'phaser';
import msgpack from 'msgpack-lite';

import ServerSocket from '../protocol/ServerSocket';
import Player from '../model/Player';
import NET_MSGS from '../../../common/NET_MSGS';
import commonConfig from '../../../common/commonConfig';
import Bot from '../model/Bot';
import {
  checkPlayersAlive,
  checkBulletsArmed,
  checkBulletAndPlayerAlive,
  bulletsCollide,
  checkPlayerAlive, checkBulletArmed
} from '../utils/Collisions';
import findEmpty from '../utils/findEmpty';
import Meteor from '../model/Meteor';
import consoleMsg from '../../../common/consoleMsg';


const INITIAL_METEORS = 10;
const INITIAL_BOTS = 4;
const BOT_UPDATE_EVERY = 15;

const RESPAWN_DIALOG_TIME = 2;
const POWER_REUSE_TIME = 5;
const INACTIVE_KICK_TIME = 30;
const ACCEL_TIME = 1;

class Game extends Phaser.Scene {

  constructor() {
    super({key: 'Game'});

  }

  preload() {
    this.load.image('img', 'assets/sprites/player1.png');
    this.load.image('bullet', 'assets/sprites/bullet.png');
  }

  newPlayer(playerInfo, internalId) {
    const player = new Player(this, playerInfo.x, playerInfo.y, this.bullets, this.railgunBullets);
    player.playerId = playerInfo.playerId;
    player.internalId = internalId;
    player.playerName = playerInfo.playerName;
    this.players.add(player);
    this.balanceBots();
  }

  removePlayer(playerId) {
    this.players.getChildren().forEach((player) => {
      if (playerId === player.playerId) {
        player.destroy();
      }
    });
    this.balanceBots();
  }

  playerInput (playerId, inputMSG) {
    const bufView = new Uint8Array(inputMSG);
    const inputData = msgpack.decode(bufView);
    const input =  {
      left: inputData[0],
      right: inputData[1],
      up: inputData[2],
      down: inputData[3],
      space: inputData[4],
      xKey: inputData[5],
      cKey: inputData[6]
    };
    this.players.getChildren().filter(p => playerId === p.playerId)[0].input.push(input);
  }

  msgToKiller(victimId) {
    this.players.getChildren().filter(p => victimId === p.playerId)[0].sentVictimMessage = true;
  }

  createHandlers() {
    const handlers = [];
    handlers[NET_MSGS.PLAYER.NEW] = this.newPlayer.bind(this);
    handlers[NET_MSGS.DISCONNECT] = this.removePlayer.bind(this);
    handlers[NET_MSGS.PLAYER.INPUT] = this.playerInput.bind(this);
    handlers[NET_MSGS.PLAYER.MSG_TO_KILLER] = this.msgToKiller.bind(this);
    return handlers;
  }

  setupMeteors() {
    this.meteors.clear(true, true);
    this.socket.clearMeteors();
    for (let i = 0; i < INITIAL_METEORS; i++) {
      const {newX, newY } = findEmpty(this.players, this.meteors);

      const meteor = new Meteor(this, newX, newY, this._meteorId++);
      this.meteors.add(meteor);
      this.socket.initMeteorInfo(meteor.getInfo());
    }
  }

  balanceBots() {
    const botsNo = this.players.getChildren().filter(p => p.bot).length;
    const playersNo = this.players.getChildren().filter(p => !p.bot).length;

    const desiredNoOfPlayers = 5;
    if (playersNo + botsNo < desiredNoOfPlayers) {
      for (let i = 0; i < desiredNoOfPlayers - (playersNo + botsNo); i++) {
        this.addBot();
      }
    }
    if (playersNo + botsNo > desiredNoOfPlayers) {
      for (let i = 0; i < (playersNo + botsNo) - desiredNoOfPlayers; i++) {
        const botToRemove = this.players.getChildren().filter(p => p.bot)[0];
        this.socket.removeBot(botToRemove.playerId);
        botToRemove.destroy();
      }
    }
  }

  setupBots() {
    for (let i = 0; i < INITIAL_BOTS; i++) {
      this.addBot();
    }
  }

  getBotId() {
    const botId = `Bot${this._botId++}`;
    if (this._botId > 100) {
      this._botId = 0;
    }
    return botId;
  }

  addBot() {
    const {newX, newY} = findEmpty(this.players, this.meteors);

    const bot = new Bot(this, newX, newY, this.bullets, this.railgunBullets);
    const id = this.getBotId();
    bot.playerId = id;
    bot.playerName = id;
    this.players.add(bot);
    if (this.socket) {
      this.socket.initBotInfo(bot.playerId, newY, newY);
    }
  }

  playerHitAction(player, bullet) {
    bullet.hit();
    if (!this._warmup) {
      player.damage();

      if (player.life <= 0) {
        console.log(`Player ${player.playerName} killed by ${bullet.parent.playerName}`);
        bullet.parent.addPoint();
        bullet.parent.streak += 1;
        this.socket.sendKillInfo(player, bullet.parent, this._firstBlood, bullet.getType());
        if (!this._firstBlood) {
          this._firstBlood = true;
          this.socket.sendConsoleMsg(consoleMsg.FIRST_BLOOD, {killer: bullet.parent.playerName});
        }
        if (bullet.parent.streak == commonConfig.game.streaks[2]) {
          this.socket.sendConsoleMsg(consoleMsg.GO_BERSERK, {killer: bullet.parent.playerName});
        }
        if (player.streak >= commonConfig.game.streaks[2]) {
          this.socket.sendConsoleMsg(consoleMsg.BERSERK_OFF, {killer:bullet.parent.playerName, victim: player.playerName});
        }
        player.streak = 0;
        if (player.bot && !bullet.parent.bot) {
          this.socket.sendBotMsgToKiller(bullet.parent.playerId, player.playerId, Math.floor(Math.random() * 3));
          player.sentVictimMessage = true;
        }
      }
    }
  }

  meteorHitAction(meteor, bullet) {
    bullet.hit();
    meteor.hit();
    if (meteor.fraction > 0) {
      const info = meteor.getInfo();
      const offSprint = new Meteor(this, info.x, info.y, this._meteorId++);
      offSprint.fraction = info.fraction;
      offSprint.setVelocity(-meteor.body.velocity.x, -meteor.body.velocity.y);
      this.meteors.add(offSprint);
      this.socket.initMeteorInfo(offSprint.getInfo());
    }
  }

  create() {
    this.players = this.add.group();
    this.bullets = this.add.group();
    this.meteors = this.add.group();
    this.railgunBullets = this.add.group();
    this._botId = 0;
    this._time = 0;
    this.lastBotUpdate = 0;
    this._emptyFor = 0;
    this._firstBlood = false;
    this._meteorId = 0;
    this._warmup = false;
    this.socket = new ServerSocket(window.ioNamespace, commonConfig.game.size.width, commonConfig.game.size.height);
    this.socket.init(this.createHandlers());
    this.physics.world.setBounds(0, 0, commonConfig.game.size.width, commonConfig.game.size.height);

    this.setupBots();
    this.setupMeteors();

    this.physics.add.collider(this.players, this.bullets, this.playerHitAction.bind(this), checkBulletAndPlayerAlive);
    this.physics.add.collider(this.meteors, this.bullets, this.meteorHitAction.bind(this), checkBulletArmed);
    this.physics.add.collider(this.players, this.players, null, checkPlayersAlive);
    this.physics.add.collider(this.players, this.meteors, null, checkPlayerAlive);
    this.physics.add.collider(this.bullets, this.bullets, checkBulletsArmed, bulletsCollide);
    this.physics.add.overlap(this.players, this.railgunBullets, this.playerHitAction.bind(this), checkBulletAndPlayerAlive);
    this.physics.add.overlap(this.meteors, this.railgunBullets, this.meteorHitAction.bind(this), checkBulletArmed);
    this.physics.add.collider(this.meteors, this.meteors, null, null);

    window.getPlayerNo = () => {
      return this.players.getChildren().filter(p => !p.bot).length;
    };

    window.getNamespace = () => {
      return window.ioNamespace;
    };

    window.setTimer = this.setTimer.bind(this);
    window.getResults = this.getResults.bind(this);
    window.sendMaintenanceMsg = this.sendMaintenanceMsg.bind(this);
    window.sendPlayers = this.sendPlayers.bind(this);

    if (window.onGameLoaded) {
      window.onGameLoaded(window.ioNamespace);
    }

  }

  sendMaintenanceMsg() {
    this.socket.sendConsoleMsg(consoleMsg.MAINTENANCE);
  }

  sendPlayers(number, namespace) {
    this.players.getChildren().filter(p => !p.bot).sort((a, b) => b.points - a.points)
      .slice(0, number).forEach(p => {
        this.socket.sendPlayerTo(p.playerId, namespace);
        console.log(`sending ${p.playerId} player to ${namespace}`);
      });
  }

  getResults() {
    let maxScore = 0;
    this.players.getChildren().forEach(p => {
      if (p.points > maxScore) {
        maxScore = p.points;
      }
    });
    const results =  this.players.getChildren().filter(p => p.internalId && p.points).map(p => {
      return {id: p.internalId,
        points: p.points
      };
    });
    this.socket.sendConsoleMsg(consoleMsg.RESULTS_SENT);
    return {results, maxScore};
  }

  setTimer(time) {
    this.socket.sendTime(time);
    this._time = time;
    if (this._time > commonConfig.game.times.round && !this._warmup) {
      this.socket.sendWinners();
    }
    this._warmup = this._time > commonConfig.game.times.round;
    if (this._time == commonConfig.game.times.round) {
      this.resetRound();
    }

    if (this.players.getChildren().filter(p => !p.bot).length == 0) {
      this._emptyFor++;
    } else {
      this._emptyFor = 0;
    }

    if (this._emptyFor > 60) {
      window.removeGame(window.ioNamespace);
    }
  }

  resetRound() {
    this.players.getChildren().forEach((player) => {
      const {newX, newY } = findEmpty(this.players, this.meteors);
      player.setVelocity(0);
      player.reset(newX, newY);
      player.points = 0;
      this.socket.setPlayerInfo(player.playerId, player.getInfo());
    });
    this.socket.sendRanking();
    this._meteorId = 0;
    this._firstBlood = false;
    this.setupMeteors();
  }

  update(time) {
    const turnSpeed = 275;
    const turnDelayFactor = 0.65;
    const forwardSpeed = 300;
    const accelerationSpeed = 30000;
    const backwardSpeed = -250;

    this.players.getChildren().forEach((player) => {
      if (player.bot && time - this.lastBotUpdate > BOT_UPDATE_EVERY) {
        if (player.updateState(time, this.players.getChildren().filter(p => p.playerId !== player.playerId))) {
          this.lastBotUpdate = time;
        }
      } else if (typeof player.lastActiveTime == 'undefined') {
        player.lastActiveTime = time;
      }

      if (player.input.length == 0) {
        if (time - player.lastActiveTime > INACTIVE_KICK_TIME*1000) {
          this.socket.kickPlayer(player.playerId);
          return;
        }
      } else {
        player.lastActiveTime = time;
      }

      if (player.life > 0) {
        if (player.protection) {
          if (player.input.length > 0) {
            player.protection = false;
          }
        }

        while (player.input.length > 0) {
          const input = player.input.pop();
          const speed = player.body.speed;
          if (input.left) {
            player.setAngularVelocity(-turnSpeed + (speed * turnDelayFactor));
          } else if (input.right) {
            player.setAngularVelocity(turnSpeed - (speed * turnDelayFactor));
          } else {
            player.setAngularVelocity(0);
          }

          if (input.up) {
            this.physics.velocityFromRotation(player.rotation, forwardSpeed, player.body.acceleration);
            player.engines = 1;
          } else if (input.down) {
            this.physics.velocityFromRotation(player.rotation, backwardSpeed, player.body.acceleration);
            player.engines = -1;
          } else {
            player.setAcceleration(0);
            player.engines = 0;
          }

          if (input.space && !this._warmup) {
            input.space = false;
            player.fireBullet();
          }

          if (input.xKey && player.powered && !this._warmup) {
            input.xKey = false;
            player.powered = false;
            player.timeOfPowerUse = time;
            player.setMaxVelocity(280);
            this.physics.velocityFromRotation(player.rotation, accelerationSpeed, player.body.acceleration);
            this.socket.sendAccelAction(player.playerId);

          }

          if (input.cKey && player.powered && !this._warmup) {
            input.cKey = false;
            player.powered = false;
            player.timeOfPowerUse = time;
            player.fireRailgun(this.players.getChildren().filter(p => p.playerId !== player.playerId),
              this.playerHitAction.bind(this),
              this.meteors.getChildren(),
              this.meteorHitAction.bind(this));
            this.socket.sendRailgunShot(player);

          }
        }

        if (player.timeOfPowerUse && time - player.timeOfPowerUse > ACCEL_TIME*1000) {
          player.setMaxVelocity(200);
        }

        if (player.timeOfPowerUse && time - player.timeOfPowerUse > POWER_REUSE_TIME*1000) {
          player.powered = true;
          player.timeOfPowerUse = null;
        }

      } else {
        player.setVelocity(0);
        player.setAngularVelocity(0);
        if (!player.bot) {
          player.input = [];
        }
        if (!player.timeOfDead) {
          player.timeOfDead = time;
          player.sentVictimMessage = false;
        }
        if (time - player.timeOfDead > RESPAWN_DIALOG_TIME*1000 && player.life == 0) {
          const {newX, newY} = findEmpty(this.players, this.meteors);
          player.x = newX;
          player.y = newY;
          player.protection = true;
          player.powered = true;
          player.life = -1;
        }
        if (time - player.timeOfDead >
          (RESPAWN_DIALOG_TIME+commonConfig.game.times.respawnTime)*1000 || player.sentVictimMessage) {
          player.reset();
        }
      }


      this.socket.setPlayerInfo(player.playerId, player.getInfo());

    });

    this.meteors.getChildren().forEach((meteor) => {
      if (meteor.fraction <= 0) {
        this.socket.removeMeteor(meteor.getInfo());
        meteor.destroy();
      }
    });

    this.meteors.getChildren().forEach((meteor) => {
      this.socket.setMeteorInfo(meteor.getInfo());

    });
    this.socket.sendUpdate();
    //console.log(this.game.loop.actualFps);

  }
}
export default Game;
