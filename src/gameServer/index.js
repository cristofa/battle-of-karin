import app, {io, httpServer} from './server';
import commonConfig from '../common/commonConfig';

const startServer = (app, httpServer, io) => {
  return new Promise((resolve, reject) => {
    httpServer.once('error', (err) => {
      if (err.code === 'EADDRINUSE') {
        reject(err);
      }
    });
    httpServer.once('listening', () => { console.log('LISTENING'); resolve(httpServer); });

  }).then((httpServer) => {
    console.info(`==> Listening on ${commonConfig.gameApiPort}. Open up http://localhost:${commonConfig.gameApiPort}/ in your browser.`);

    // Hot Module Replacement API
    if (module.hot) {
      let currentApp = app;
      module.hot.accept('./server', () => {
        io.removeListener('request', currentApp);
        httpServer.removeListener('request', currentApp);
        import('./server')
          .then(({default: nextApp}) => {
            currentApp = nextApp;
            io.on('request', currentApp);
            httpServer.on('request', currentApp);
            console.log('HttpServer reloaded!');
          })
          .catch((err) => console.error(err));
      });

      // For reload main module (self). It will be restart http-server.
      module.hot.accept((err) => console.error(err));
      module.hot.dispose(() => {
        console.log('Disposing entry module...');
        httpServer.close();
      });
    }
  });
};

console.log('Starting http server...');
startServer(app, httpServer, io).catch((err) => {
  console.error('Error in server start script.', err);
});
