import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import { BasicStrategy } from 'passport-http';

import setupRouter from './controllers';
import config from './config';
import commonConfig from '../common/commonConfig';

const app = express();
const httpServer = app.listen(commonConfig.gameApiPort);

const io = require('socket.io')(httpServer, {
  wsEngine: require('eiows').Server,
  perMessageDeflate: {
    threshold: 32768
  }
});

passport.use(new BasicStrategy(
  function(user, password, done) {
    if (user == config.gameApiUser && password == config.gameApiPass) {
      return done(null, true);
    }
    return done(null, false);
  }
));

app.use(passport.initialize());

app.use(express.static('public'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


const corsOptions = {};
if (process.env.NODE_ENV !== 'development') {
  corsOptions.origin = `https://${commonConfig.host}`;
  corsOptions.credentials = true;
}
app.use(cors(corsOptions));


if (process.env.NODE_ENV === 'development') {
  app.use(express.static('build-game'));
  app.use(express.static('build-client'));
}
app.use('/', setupRouter(io));

export { io, httpServer };

export default app;
