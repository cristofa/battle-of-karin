export default {
  sessionSecret: process.env.SESSION_SECRET || 'sessionsecret',
  dbName: process.env.DB_NAME || 'karin',
  dbUser: process.env.DB_NAME || 'karin',
  dbPass: process.env.DB_NAME || 'karin',
  dbHost: process.env.DB_HOST || 'localhost',
  dbPort: process.env.DB_PORT || '5432',
  discordClientId: process.env.DISCORD_CLIENT_ID,
  discordSecret: process.env.DISCORD_SECRET,
  maxPlayersPerGame: 2,
  balancedGameSize: 2,
  emptyGameSize: 3,
  maxGames: 4,
  gameApiUser: process.env.GAME_API_USER || 'karin',
  gameApiPass: process.env.GAME_API_PASS || 'karin',
  basicAuthUser: process.env.BASIC_AUTH_USER || 'karin',
  basicAuthPass: process.env.BASIC_AUTH_PASS || 'karin'
};
