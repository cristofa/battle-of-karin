import express from 'express';

import GameCtrl from './GameCtrl';

const setupRouter = (io) => {

  const router = express.Router();
  const gameCtrl = new GameCtrl(io);
  router.use('/game', gameCtrl.controller);

  return router;
};

export default setupRouter;
