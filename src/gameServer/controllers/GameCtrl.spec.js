import { describe } from '@jest/globals';

import GameCtrl from './GameCtrl';
import config from '../config';

let gameCtrl;
beforeAll(() => {
  gameCtrl = new GameCtrl();
  gameCtrl.startGame = jest.fn();
  config.balancedGameSize = 10;
  config.emptyGameSize = 5;
});

class DomMock {
  constructor(surplus, ns = Math.random()) {
    this.playersNo = config.balancedGameSize + surplus;
    this.namespace = ns;
  }
  addPlayers(number) {
    this.playersNo += number;
  }

  getNamespace() {
    return this.namespace;
  }
  getPlayerNo() {
    return this.playersNo;
  }
  sendPlayers(number, ns) {
    this.playersNo -= number;
    gameCtrl._dom.find(d => d.getNamespace() == ns).addPlayers(number);
  }
}
const getDom = (playersSurplus, ns) => {
  const dom = new DomMock(playersSurplus, ns);
  dom.sendPlayersSpy = jest.spyOn(dom, 'sendPlayers');
  return dom;
};

describe('GameCtrl balancing', () => {

  test('switch player from full to empty', async () => {
    const domFull = getDom(2);
    const domEmpty = getDom(-2);

    gameCtrl._dom = [domFull, domEmpty];
    await gameCtrl.balanceGames();
    expect(domFull.sendPlayers).toHaveBeenCalledWith(2, domEmpty.getNamespace());
    expect(domEmpty.sendPlayers).not.toHaveBeenCalled();
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Do nothing when both are balanced', async () => {
    const domFull = getDom(-4);
    const domEmpty = getDom(-2);

    gameCtrl._dom = [domFull, domEmpty];

    await gameCtrl.balanceGames();
    expect(domFull.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty.sendPlayers).not.toHaveBeenCalled();
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Clear empty if enough room', async () => {

    const domFull = getDom(-4);
    const domEmpty = getDom(-6);

    gameCtrl._dom = [domFull, domEmpty];
    await gameCtrl.balanceGames();
    expect(domFull.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty.sendPlayers).toHaveBeenCalledWith(4, domFull.getNamespace());
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Do nothing if one has 0 players', async () => {

    const domEmpty = getDom(-8);
    const domZero = getDom(-config.balancedGameSize);

    gameCtrl._dom = [domZero, domEmpty];
    await gameCtrl.balanceGames();
    expect(domZero.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty.sendPlayers).not.toHaveBeenCalled();
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Clear empty if enough room in multiple doms', async () => {
    const domFull1 = getDom(-2);
    const domFull2 = getDom(-2);
    const domEmpty = getDom(-7);

    gameCtrl._dom = [domFull1, domFull2, domEmpty];
    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull2.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty.sendPlayers).toHaveBeenCalledWith(2, domFull1.getNamespace());
    expect(domEmpty.sendPlayers).toHaveBeenCalledWith(1, domFull2.getNamespace());
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Clear multiple empty if enough room in multiple doms', async () => {

    const domFull1 = getDom(-2);
    const domFull2 = getDom(-3);
    const domEmpty1 = getDom(-7);
    const domEmpty2 = getDom(-8);

    gameCtrl._dom = [domFull1, domFull2, domEmpty1, domEmpty2];
    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull2.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty1.sendPlayers).toHaveBeenCalledWith(2, domFull1.getNamespace());
    expect(domEmpty1.sendPlayers).toHaveBeenCalledWith(1, domFull2.getNamespace());
    expect(domEmpty2.sendPlayers).toHaveBeenCalledWith(2, domFull2.getNamespace());
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Move from multiple empty to multiple full', async () => {

    const domFull1 = getDom(0);
    const domFull2 = getDom(-1);
    const domFull3 = getDom(-2);
    const domFull4 = getDom(-3);
    const domEmpty1 = getDom(-7);
    const domEmpty2 = getDom(-8);
    const domEmpty3 = getDom(-9);

    gameCtrl._dom = [domFull2, domFull3, domFull1, domFull4, domEmpty2, domEmpty1, domEmpty3];
    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull2.sendPlayers).not.toHaveBeenCalled();
    expect(domFull3.sendPlayers).not.toHaveBeenCalled();
    expect(domFull4.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty1.sendPlayers).toHaveBeenCalledWith(1, domFull2.getNamespace());
    expect(domEmpty1.sendPlayers).toHaveBeenCalledWith(2, domFull3.getNamespace());
    expect(domEmpty2.sendPlayers).toHaveBeenCalledWith(2, domFull4.getNamespace());
    expect(domEmpty3.sendPlayers).toHaveBeenCalledWith(1, domFull4.getNamespace());
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Full and empty at the same time', async () => {

    const domFull1 = getDom(3);
    const domFull2 = getDom(1);
    const domFull3 = getDom(0);
    const domFull4 = getDom(-1);
    const domEmpty1 = getDom(-7);
    const domEmpty2 = getDom(-8);
    const domEmpty3 = getDom(-9);

    gameCtrl._dom = [domFull2, domFull3, domFull1, domFull4, domEmpty2, domEmpty1, domEmpty3];

    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).toHaveBeenCalledWith(1, domFull4.getNamespace());
    expect(domFull1.sendPlayers).toHaveBeenCalledWith(2, domEmpty1.getNamespace());
    expect(domFull2.sendPlayers).toHaveBeenCalledWith(1, domEmpty1.getNamespace());
    expect(domFull3.sendPlayers).not.toHaveBeenCalled();
    expect(domFull4.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty1.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty2.sendPlayers).toHaveBeenCalledWith(2, domEmpty1.getNamespace());
    expect(domEmpty3.sendPlayers).toHaveBeenCalledWith(1, domEmpty1.getNamespace());
    expect(gameCtrl.startGame).not.toHaveBeenCalled();
  });

  test('Edge case - empty without free slots', async () => {

    const domFull1 = getDom(0);
    const domFull2 = getDom(1);
    const domEmpty1 = getDom(-9);

    gameCtrl._dom = [domEmpty1, domFull2, domFull1];

    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull2.sendPlayers).toHaveBeenCalledWith(1, domEmpty1.getNamespace());
    expect(domEmpty1.sendPlayers).not.toHaveBeenCalled();

    expect(domFull1.getPlayerNo()).toBe(10);
    expect(domFull2.getPlayerNo()).toBe(10);
    expect(domEmpty1.getPlayerNo()).toBe(2);

  });

  test('Do nothing when all full', async () => {

    const domFull1 = getDom(3);
    const domFull2 = getDom(1);
    const domFull3 = getDom(3);

    gameCtrl._dom = [domFull2, domFull3, domFull1];

    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull2.sendPlayers).not.toHaveBeenCalled();
    expect(domFull3.sendPlayers).not.toHaveBeenCalled();
  });

  test('Clear when all empty', async () => {

    const domEmpty1 = getDom(-7);
    const domEmpty2 = getDom(-8);
    const domEmpty3 = getDom(-9);

    gameCtrl._dom = [domEmpty1, domEmpty3, domEmpty2];

    await gameCtrl.balanceGames();
    expect(domEmpty1.sendPlayers).toHaveBeenCalledWith(3, domEmpty2.getNamespace());
    expect(domEmpty2.sendPlayers).not.toHaveBeenCalled();
    expect(domEmpty3.sendPlayers).toHaveBeenCalledWith(1, domEmpty2.getNamespace());

    expect(domEmpty1.getPlayerNo()).toBe(0);
    expect(domEmpty2.getPlayerNo()).toBe(6);
    expect(domEmpty3.getPlayerNo()).toBe(0);
  });

  test('Rebalance with one empty dom', async () => {

    const domEmpty1 = getDom(-8, 'empty1');
    gameCtrl._dom = [domEmpty1];
    await gameCtrl.balanceGames();
    expect(domEmpty1.sendPlayers).not.toHaveBeenCalled();

  });

  test('Rebalance with multiple empty and full', async () => {

    const domEmpty1 = getDom(-1, 'empty1');
    const domEmpty2 = getDom(-2, 'empty2');
    const domEmpty3 = getDom(-6, 'empty3');
    const domFull1 = getDom(0, 'full1');
    const domFull2 = getDom(2, 'full2');
    const domFull3 = getDom(4, 'full3');

    gameCtrl._dom = [domEmpty1, domEmpty3, domEmpty2, domFull1, domFull2, domFull3];

    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull3.sendPlayers).toHaveBeenCalledWith(1, domEmpty1.getNamespace());
    expect(domFull3.sendPlayers).toHaveBeenCalledWith(2, domEmpty2.getNamespace());
    expect(domFull3.sendPlayers).toHaveBeenCalledWith(1, domEmpty3.getNamespace());
    expect(domFull2.sendPlayers).toHaveBeenCalledWith(2, domEmpty3.getNamespace());

    expect(domEmpty1.getPlayerNo()).toBe(10);
    expect(domEmpty2.getPlayerNo()).toBe(10);
    expect(domEmpty3.getPlayerNo()).toBe(7);
    expect(domFull1.getPlayerNo()).toBe(10);
    expect(domFull2.getPlayerNo()).toBe(10);
    expect(domFull3.getPlayerNo()).toBe(10);
  });

  test('Do nothing when balanced', async () => {
    const domFull1 = getDom(0, 'full1');
    const domFull2 = getDom(0, 'full2');
    const domFull3 = getDom(-2, 'full3');

    gameCtrl._dom = [domFull1, domFull2, domFull3];
    await gameCtrl.balanceGames();
    expect(domFull1.sendPlayers).not.toHaveBeenCalled();
    expect(domFull2.sendPlayers).not.toHaveBeenCalled();
    expect(domFull3.sendPlayers).not.toHaveBeenCalled();

  });

});
