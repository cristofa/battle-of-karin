import express from 'express';
import jsdom from 'jsdom';
import Datauri from 'datauri';
import fetch from 'node-fetch';
import passport from 'passport';
import base64 from 'base-64';

import config from '../config';
import path from 'path';
import {ERROR} from '../../common/constants';
import getBackendURI from '../game/utils/getBackendURI';
import commonConfig from '../../common/commonConfig';

class GameCtrl {
  _controller = express.Router();
  _io;
  _datauri = new Datauri();
  _dom = [];
  _timer;
  _time = 0;

  constructor(io) {
    this._io = io;
    this._roundTime = commonConfig.game.times.round;
    this._warmupTime = commonConfig.game.times.warmup;

    this._controller.get('/maintenance', passport.authenticate('basic', { session: false }),  (req, res) => {
      console.log('Show maintenance message...');
      this._dom.forEach(d => {
        d.window.sendMaintenanceMsg();
      });
      res.send('Maintenance message sent');
      return;
    });

    this._controller.get('/reload', passport.authenticate('basic', { session: false }),  (req, res) => {
      if (process.env.NODE_ENV !== 'development') {
        res.send('Dev only...');
        return;
      }

      console.log('Reloading game...');
      this._dom.forEach(d => {
        this._io.of(d.window.getNamespace()).removeAllListeners();
        d.window.close();
      });
      this._dom = [];
      res.send('Reloading game...');
      return;
    });

    this._controller.get('/namespace/:uuid?',  async (req, res) => {
      let namespace;
      if (req.params.uuid) {
        for (const d of this._dom) {
          if (req.params.uuid == d.window.getNamespace() && d.window.getPlayerNo() < config.maxPlayersPerGame) {
            namespace = req.params.uuid;
            res.status(200);
            res.json({ns: namespace});
            return;
          }
        }
      }

      for (const d of this._dom) {
        if (d.window.getPlayerNo() < config.maxPlayersPerGame) {
          namespace = d.window.getNamespace();
          res.status(200);
          res.json({ns: namespace});
          return;
        }
      }

      if (!namespace) {
        if (this._dom.length < config.maxGames) {
          namespace = this.getNamespace();
          this.startGame(namespace).then(() => {
            res.status(200);
            res.json({ns: namespace});

            if (!this._timer) {
              this.startTimer();
            }
          }).catch(() => {
            res.status(500);
            res.json({error: ERROR.general});
          });

        } else {
          console.log('ERROR: server full.');
          res.status(503);
          res.json({error: ERROR.serverFull});
        }
      }
    });
  }

  getNamespace() {
    return `game_${this._dom.length}`;
  }

  startTimer() {
    this._time = this._warmupTime + this._roundTime;
    this._timer = setInterval(() => {
      if (this._dom.length == 0) {
        clearInterval(this._timer);
        this._timer = null;
      }
      this._time--;
      this._dom.forEach(d => {
        d.window.setTimer(this._time);
      });

      if (this._time === 0) {
        this._time = this._warmupTime + this._roundTime;
        this.saveAllResults().then(() => {
          console.log('Results saved');
        });
      }
      if (this._time == (this._roundTime + this._warmupTime/2)) {
        console.log('Before rebalance: ' + this._dom.map(d => d.getPlayerNo()));
        this.balanceGames().then(() => {
          console.log('Games rebalanced. ');
        });
      }
    }, 1000);

  }

  totalFreeSlots(playersNo) {
    const freeSlots = config.balancedGameSize - playersNo;
    return this._dom.reduce((total, d) => total + Math.max(0, config.balancedGameSize - d.getPlayerNo()), -freeSlots);
  }

  async balanceGames() {
    let fullFirst = this._dom.sort((a, b) => b.getPlayerNo() - a.getPlayerNo());
    fullFirst.forEach(d => {
      const playersNo = d.getPlayerNo();
      let freeSlots = this.totalFreeSlots(playersNo);
      if (playersNo > config.balancedGameSize) {
        let playersToSwitch = d.getPlayerNo() - config.balancedGameSize;
        if (playersToSwitch <= freeSlots) {
          this.movePlayerFrom(d, playersToSwitch, false);
        }
      } else if (playersNo < config.emptyGameSize) {
        if (playersNo <= freeSlots) {
          let playersToSwitch = playersNo;
          this.movePlayerFrom(d, playersToSwitch, true);
        }
      }
    });

    this._dom.sort((a, b) => b.getPlayerNo() - a.getPlayerNo());

  }

  movePlayerFrom(dom, playersToSwitch, movingFromEmpty) {
    const fullFirst = this._dom.map(d => { return {playerNo: d.getPlayerNo(), ns: d.getNamespace()} ;})
      .sort((a, b) => b.playerNo - a.playerNo);

    for (let i = 0; i < fullFirst.length && playersToSwitch > 0; i++) {
      if (dom.getNamespace() !== fullFirst[i].ns && (!movingFromEmpty || fullFirst[i].playerNo > 0)) {
        let canAccept = config.balancedGameSize - fullFirst[i].playerNo;
        if (canAccept > 0) {
          if (canAccept > playersToSwitch) {
            canAccept = playersToSwitch;
          }
          dom.sendPlayers(canAccept, fullFirst[i].ns);
          playersToSwitch -= canAccept;
        }
      }
    }
  }

  async saveAllResults() {
    for (let d of this._dom) {
      await this.saveResults(d.window.getResults());
    }
    await this.refreshStats();
  }

  async refreshStats() {
    await fetch(`${getBackendURI()}/result/refresh`, {
      headers: {
        'Authorization': `Basic ${base64.encode(config.basicAuthUser + ':' + config.basicAuthPass)}`
      }
    }).then(res => {
      if (!res.ok) {
        throw new Error('Can\'t refresh stats: ' + res.status);
      }
    }).catch(e => {
      console.log(e.message);
    });
  }

  async saveResults(results) {
    await fetch(`${getBackendURI()}/result/`, {
      method: 'POST',
      body: JSON.stringify(results),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${base64.encode(config.basicAuthUser + ':' + config.basicAuthPass)}`
      }
    }).then(res => {
      if (!res.ok) {
        throw new Error('Can\'t save results: ' + res.status);
      }
    }).catch(e => {
      console.log(e.message);
    });
  }

  async getPlayersData(playerId) {
    let json = await fetch(`${getBackendURI()}/user/${playerId}`,
      {
        headers: {
          'Authorization': `Basic ${base64.encode(config.basicAuthUser + ':' + config.basicAuthPass)}`
        }
      })
      .then(res => res.json()).then(json => json)
      .catch(e => {
        console.log(e.message);
      });
    return json;
  }


  async startGame(namespace) {
    const {JSDOM} = jsdom;
    let gamePath;
    if (process.env.NODE_ENV === 'development') {
      gamePath = path.resolve('./build-game/server_index.html');
    } else {
      gamePath = path.resolve('./game/server_index.html');
    }
    return new Promise((resolve, reject) => {
      JSDOM.fromFile(gamePath, {
        runScripts: 'dangerously',
        resources: 'usable',
        pretendToBeVisual: true
      }).then((dom) => {
        dom.window.ioNamespace = namespace;
        dom.window.getPlayersData = this.getPlayersData;
        dom.window.URL.createObjectURL = (blob) => {
          if (blob) {
            return this._datauri.format(blob.type, blob[Object.getOwnPropertySymbols(blob)[0]]._buffer).content;
          }
        };
        // eslint-disable-next-line no-unused-vars
        dom.window.URL.revokeObjectURL = (objectURL) => {
        };
        dom.window.onGameLoaded = (ns) => {
          dom.getPlayerNo = dom.window.getPlayerNo;
          dom.sendMaintenanceMsg = dom.window.sendMaintenanceMsg;
          dom.sendPlayers = dom.window.sendPlayers;
          dom.getNamespace = dom.window.getNamespace;
          this._dom.push(dom);
          console.log(`GAME ${ns} LOADED!`);
          resolve();
        };

        dom.window.removeGame = (ns) => {
          console.log(`Closing game ${ns}`);
          const domIndex = this._dom.findIndex(d => d.window.getNamespace() == ns);
          if (domIndex >= 0) {
            this._io.of(this._dom[domIndex].window.getNamespace()).removeAllListeners();
            this._dom[domIndex].window.close();
            this._dom.splice(domIndex, 1);
          }
        };

        dom.window.io = this._io;
      }).catch((error) => {
        console.log(error.message);
        reject();
      });
    });
  }


  get controller() {
    return this._controller;
  }
}

export default GameCtrl;
