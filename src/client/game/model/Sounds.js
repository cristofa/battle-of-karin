const sounds = {
  SHOT: 'shot',
  EXPLODE: 'explode',
  HIT: 'hit',
  ACCEL: 'accel',
  POWERED: 'powered',
  RAILGUN: 'railgun',
  METEOR: 'meteor',
  BULLET: 'bullet',
  INTRO: 'intro',
  COUNTDOWN: 'countdown',
  THRESHOLD0: 'threshold0',
  THRESHOLD1: 'threshold1',
  THRESHOLD2: 'threshold2',
  THRESHOLD3: 'threshold3',
  BACKGROUND: 'background'
};

export default sounds;
