import Phaser from 'phaser';

import {getEmitterConfig} from '../../utils';
import {randomColor} from '../../../common/commonUtils';
import Bullet from './Bullet';
import sounds from './Sounds';


class ClientPlayer extends Phaser.GameObjects.Container {
  _playerId;
  _id;
  _playerName;
  _light;
  _bullets;
  _dead;
  _life;
  _sentMessage;
  _protection = false;
  _powered = false;
  _protectionTween;

  constructor(scene, x, y, playerId, playerName, id) {
    super(scene, x, y);
    this._playerId = playerId;
    this._id = id;
    this._playerName = playerName;

    this.sndClb = scene.emitSoundIfVisible.bind(scene);

    this._dead = false;
    this._sentMessage = false;

    scene.add.existing(this);
    this._scene = scene;

    this._light = scene.add.pointlight(-5, 0, 0, 90, 1);
    const r = Math.random() > 0.5 ? 0 : 1;
    const g = Math.random() > 0.5 ? 0 : 1;
    const b = !r && !g ? 1 : Math.random() > 0.5 ? 0 : 1;
    this._lightColor = {r, g, b};
    this._light.color.setFromRGB(this._lightColor);
    this._light.attenuation = 0.5;
    this.add(this._light);

    this._body = this._scene.add.sprite(0, 0, 'player', 0);
    this._body.setScale(0.4);
    this.add(this._body);
    this._cockpit = this._scene.add.sprite(0, 0, 'cockpit');
    this._cockpit.setScale(0.4);
    this.add(this._cockpit);
    this._cockpit2 = this._scene.add.sprite(0, 0, 'cockpit2');
    this._cockpit2.setScale(0.4);
    this.add(this._cockpit2);
    this._body2 = this._scene.add.sprite(0, 0, 'player2');
    this._body2.setScale(0.4);
    this.add(this._body2);


    this._explodeEmitter = this._scene.add.particles(0, 0, 'ship-part', {
      frequency: -1,
      blendMode: 'ADD',
      alpha: {start: 1, end: 0, ease: 'Linear'},
      lifespan: 2500,
      emitCallback: (item) => {
        item.velocityX = -100 + Math.random()*200;
        item.velocityY = -100 + Math.random()*200;
        this._scene.tweens.add({
          targets: item,
          duration: 500+Math.random()*1000,
          rotation: 2*Math.PI,
          ease: 'Linear',
          repeat: 2
        });
      }
    });
    this._explodeEmitter.startFollow(this, -0, 0);

    this._sparksEmitter = this._scene.add.particles(0, 0, 'particle', {
      frequency: 200,
      blendMode: 'ADD',
      alpha: {start: 1, end: 0, ease: 'Linear'},
      lifespan: 2500,
      speed: 100,
      scale: 0.5
    });
    this._sparksEmitter.startFollow(this, -0, 0);
    this._sparksEmitter.stop();

    this._berserkEmitter = this._scene.add.particles(0, 0, 'particle', {
      frequency: 20,
      blendMode: 'ADD',
      alpha: {start: 1, end: 0, ease: 'Linear'},
      lifespan: 1000,
      tint: [0xff0000],
      speed: 100,
      scale: 0.5
    });
    this._berserkEmitter.startFollow(this, -0, 0);
    this._berserkEmitter.stop();

    this.updateTexture((randomColor()), (randomColor()));
    const flameColor = Phaser.Display.Color.HexStringToColor(randomColor()).color;
    const emitterConfig = getEmitterConfig(flameColor);
    const frontEmitterConfig = getEmitterConfig(flameColor, 0, 0.4);

    this._leftEmitter = this._scene.add.particles(0, 0, 'particle', emitterConfig);
    this._leftEmitter.startFollow(this, -20, -15);
    this._rightEmitter = this._scene.add.particles(0, 0, 'particle', emitterConfig);
    this._rightEmitter.startFollow(this, -20, 15);

    this._frontLeftEmitter = this._scene.add.particles(0, 0, 'particle', frontEmitterConfig);
    this._frontLeftEmitter.startFollow(this, 20, -7);
    this._frontRightEmitter = this._scene.add.particles(0, 0, 'particle', frontEmitterConfig);
    this._frontRightEmitter.startFollow(this, 20, 7);

    this._leftAccelEmitter = this._scene.add.particles(0, 0, 'particle', {
      frequency: -1,
      blendMode: 'ADD',
      tint: [flameColor],
      alpha: {start: 1, end: 0, ease: 'Linear'},
      lifespan: 1000,
      speed: 100
    });
    this._leftAccelEmitter.startFollow(this, -20, -15);
    this._rightAccelEmitter = this._scene.add.particles(0, 0, 'particle', {
      frequency: -1,
      blendMode: 'ADD',
      tint: [flameColor],
      alpha: {start: 1, end: 0, ease: 'Linear'},
      lifespan: 1000,
      speed: 100
    });
    this._rightAccelEmitter.startFollow(this, -20, 15);

    this.setScale(1);

    this._protectionTween = scene.tweens.add({
      targets: this,
      alpha: '0.2',
      ease: 'Power2',
      repeat: -1,
      yoyo: true,
      paused: true
    });

    this.setDepth(2);

  }

  get protection() {
    return this._protection;
  }

  updateTexture(bodyColorHex, cockpitColorHex) {

    const bodyColor = Phaser.Display.Color.HexStringToColor(bodyColorHex).color;
    const cockpitColor = Phaser.Display.Color.HexStringToColor(cockpitColorHex).color;

    this.tint = cockpitColor;

    this._body.tint = bodyColor;
    this._cockpit.tint = cockpitColor;

    this._explodeEmitter.setParticleTint(bodyColor);

    this._bullets = this._scene.add.group({
      key: 'bullet',
      frameQuantity: 5,
      active: false,
      visible: false,
      classType: Bullet,
      createCallback: (item) => {
        item.decorate(bodyColor, cockpitColor, this.particles);
      }
    });
    this._bullets.getChildren().forEach((b, i) => {
      b.id = i;
      b.parent = this;
    });
  }

  shotBullet() {
    this.sndClb(this.x, this.y, sounds.SHOT);
  }

  updateEmitters(flameColorHex) {
    const flameColor = Phaser.Display.Color.HexStringToColor(flameColorHex).color;
    this._leftEmitter.setParticleTint(flameColor);
    this._rightEmitter.setParticleTint(flameColor);
    this._frontLeftEmitter.setParticleTint(flameColor);
    this._frontRightEmitter.setParticleTint(flameColor);
    this._leftAccelEmitter.setParticleTint(flameColor);
    this._rightAccelEmitter.setParticleTint(flameColor);
  }

  setEngines(engines) {
    if (this._life <= 0 || this._protection) {
      this.stopEngines();
    } else {
      if (engines > 0) {
        this.goForward();
      } else if (engines < 0) {
        this.goBackward();
      } else {
        this.stopEngines();
      }
    }
  }

  goForward() {
    this._leftEmitter.start();
    this._rightEmitter.start();

    this._frontLeftEmitter.stop();
    this._frontRightEmitter.stop();
  }

  goBackward() {
    this._leftEmitter.stop();
    this._rightEmitter.stop();

    this._frontLeftEmitter.start();
    this._frontRightEmitter.start();
  }

  stopEngines() {
    this._leftEmitter.stop();
    this._rightEmitter.stop();
    this._frontLeftEmitter.stop();
    this._frontRightEmitter.stop();
  }

  setAngle(angle) {
    this.setRotation(angle);
    this._leftEmitter.followOffset = Phaser.Math.RotateAroundDistance(new Phaser.Math.Vector2(-20, -15), 0, 0, angle, 20);
    this._rightEmitter.followOffset = Phaser.Math.RotateAroundDistance(new Phaser.Math.Vector2(-20, 15), 0, 0, angle, 20);

    this._leftAccelEmitter.followOffset = Phaser.Math.RotateAroundDistance(new Phaser.Math.Vector2(-20, -15), 0, 0, angle, 20);
    this._rightAccelEmitter.followOffset = Phaser.Math.RotateAroundDistance(new Phaser.Math.Vector2(-20, 15), 0, 0, angle, 20);

    this._frontLeftEmitter.followOffset = Phaser.Math.RotateAroundDistance(new Phaser.Math.Vector2(20, 7), 0, 0, angle, 15);
    this._frontRightEmitter.followOffset = Phaser.Math.RotateAroundDistance(new Phaser.Math.Vector2(20, -7), 0, 0, angle, 15);

    const emitterAngle = Phaser.Math.Angle.Reverse(angle)*Phaser.Math.RAD_TO_DEG;
    this._leftEmitter.setEmitterAngle(emitterAngle);
    this._rightEmitter.setEmitterAngle(emitterAngle);

    this._frontLeftEmitter.setEmitterAngle(angle*Phaser.Math.RAD_TO_DEG);
    this._frontRightEmitter.setEmitterAngle(angle*Phaser.Math.RAD_TO_DEG);
    this._sparksEmitter.setEmitterAngle(angle*Phaser.Math.RAD_TO_DEG + 90);

  }

  setBullets(bullets) {
    this._bullets.getChildren().forEach((groupBullet) => {
      const id = groupBullet.id;
      if (!bullets[id]) {
        groupBullet.active = false;
        groupBullet.visible = false;
        groupBullet.armed = false;
        groupBullet.prevArmed = false;
      } else {
        groupBullet.active = bullets[id].active;
        groupBullet.visible = bullets[id].active;
        groupBullet.x = Math.floor(bullets[id].x);
        groupBullet.y = Math.floor(bullets[id].y);
        groupBullet.armed = bullets[id].armed;
      }
    });
  }

  reset() {
    this.setBerserk(false);
  }

  setBerserk(on) {
    if (on) {
      this._berserkEmitter.start();
      this._light.color.setTo(1, 0, 0);
      this._light.attenuation = 0.7;
    } else {
      this._berserkEmitter.stop();
      this._light.color.setFromRGB(this._lightColor);
      this._light.attenuation = 0.5;
    }
  }

  setLife(life) {
    if ((life == 1 || life == 2) && life != this._life) {
      this.sndClb(this.x, this.y, this.self ? sounds.BULLET : sounds.HIT);
    }

    if (life == 0 && !this._dead) {
      this.sndClb(this.x, this.y, sounds.EXPLODE);
      this._explodeEmitter.explode(80);
      this.setPartVisible(false);
      this._dead = true;
      this._sentMessage = false;
      this.setBerserk(false);
    } else if (life == -1) {
      this.setPartVisible(true);
      this._sparksEmitter.stop();
      this._body.setFrame(0);
    } else if (life > 0 && this._dead) {
      this._dead = false;
      this.setPartVisible(true);
    }
    if (life === 3) {
      this._sparksEmitter.stop();
      this._body.setFrame(0);
    } else if (life === 2) {
      this._sparksEmitter.stop();
      this._body.setFrame(1);
    } else if (life === 1) {
      this._sparksEmitter.start();
      this._body.setFrame(2);
    }

    this._life = life;
  }

  accelerate() {
    this._leftAccelEmitter.explode(20);
    this._rightAccelEmitter.explode(20);
    this.sndClb(this.x, this.y, sounds.ACCEL);
  }

  setPowered(powered) {
    if (!this._powered && powered) {
      this.sndClb(this.x, this.y, sounds.POWERED);
    }
    this._powered = powered;
    if (this._life > 0) {
      this._light.visible = powered;
    } else {
      this._light.visible = false;
    }
  }

  setProtection(protection) {
    if (!this._protection && protection) {
      this._protectionTween.play();
      this._protectionTween.restart();
      this.stopEngines();
    }
    if (this._protection && !protection) {
      this._protectionTween.pause();
      this.setAlpha(1);
    }
    this._protection = protection;
  }

  destroy() {
    super.destroy();
    this._sparksEmitter.destroy();
    this._berserkEmitter.destroy();
    this._explodeEmitter.destroy();
    this._frontLeftEmitter.destroy();
    this._frontRightEmitter.destroy();
    this._rightEmitter.destroy();
    this._leftEmitter.destroy();
    this._bullets.clear(true, true);
  }

  setPartVisible(visible) {
    //    this._sparksEmitter.killAll();
    //this._frontLeftEmitter.killAll();
    //this._frontRightEmitter.killAll();
    //this._leftEmitter.killAll();
    //this._rightEmitter.killAll();

    this._body.visible = visible;
    this._body2.visible = visible;
    this._cockpit.visible = visible;
    this._cockpit2.visible = visible;
    this._rightEmitter.visible = visible;
    this._leftEmitter.visible = visible;
    this._frontRightEmitter.visible = visible;
    this._frontLeftEmitter.visible = visible;
    this._light.visible = visible;
    this._sparksEmitter.visible = visible;
  }

  get sentMessage() {
    return this._sentMessage;
  }

  set sentMessage(value) {
    this._sentMessage = value;
  }

  get dead() {
    return this._dead;
  }

  get playerId() {
    return this._playerId;
  }

  get id() {
    return this._id;
  }

  get playerName() {
    return this._playerName;
  }
}

export default ClientPlayer;
