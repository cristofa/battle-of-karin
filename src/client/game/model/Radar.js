import Phaser from 'phaser';

import commonConfig from '../../../common/commonConfig';

const RADAR_SCALE = 30;

class Radar extends Phaser.GameObjects.Container {

  constructor(scene, x, y) {
    super(scene, x, y);
    this._graphics = scene.add.graphics({ lineStyle: { width: 2, color: 0x7a7a7a, alpha: 0.5 },
      fillStyle: {color: 0x000000, alpha: 0.5}});
    this.drawRadar();

    this._shipsGraphics = scene.add.graphics({fillStyle: {color: 0x000000, alpha: 0.5}});
    this.add(this._graphics);
    this.add(this._shipsGraphics);
  }

  updatePlayers(players) {
    this._shipsGraphics.clear();
    players.forEach(p => {
      if (!p.dead) {
        this._shipsGraphics.fillStyle(p.self ? 0x00ff00 : 0xff0000, 1);
        this._shipsGraphics.fillCircle(p.x / RADAR_SCALE, p.y / RADAR_SCALE, 2);
      }
    });
  }

  drawRadar() {
    this._graphics.lineStyle(2, 0x7a7a7a, 0.5);
    this.width = commonConfig.game.size.width/ RADAR_SCALE;
    this.height = commonConfig.game.size.height/RADAR_SCALE;
    this._x = 0;
    this._y = 0;

    this._graphics.fillRect(this._x, this._y, this.width, this.height);
    this._graphics.strokeRect(this._x, this._y, this.width, this.height);
  }
}

export default Radar;
