import Phaser from 'phaser';

const SCALE_FACTOR = 0.25;

class Meteor extends Phaser.GameObjects.Sprite {

  _fraction = 5;
  _id;
  constructor(scene, x, y, id) {
    super(scene, x, y, 'meteor', 0);
    this.setFrame(0);
    this.play({ key: 'rotate', repeat: -1 });
    scene.add.existing(this);
    this._id = id;
  }

  get fraction() {
    return this._fraction;
  }

  set fraction(value) {
    this._fraction = value;
    this.setScale(SCALE_FACTOR*value);
  }

  get id() {
    return this._id;
  }
}

export default Meteor;
