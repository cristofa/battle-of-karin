import Phaser from 'phaser';

class Bullet extends Phaser.GameObjects.Container {

  armed;
  prevArmed;

  constructor(scene, x, y) {
    super(scene, x, y);
    this._scene = scene;
    this._sprite = scene.add.sprite(5, -5, 'particle');
    this.add(this._sprite);
    this._light = scene.add.sprite(5, -5, 'particle');
    this.add(this._light);
    this._sprite.setScale(1.5);
    this.armed = false;
    this.prevArmed = false;
    this.explodeTime = 0;
    this.setDepth(1);

  }

  updateDisplayOrigin() {
  }

  preUpdate(time, delta) {
    Phaser.Actions.RotateAroundDistance([this._light], this._sprite, (180/delta)*Phaser.Math.DEG_TO_RAD, 10);
    if (this.prevArmed && !this.armed) {
      this.emitter.explode( 30, this.x, this.y);
      this._sprite.setVisible(false);
      this._light.setVisible(false);
    }
    if (!this.prevArmed && this.armed) {
      this.parent.shotBullet();
      this._sprite.setVisible(true);
      this._light.setVisible(true);
    }
    this.prevArmed = this.armed;
  }

  decorate(tint, highlightTint) {
    this._sprite.setTint(tint);
    this._light.setTint(highlightTint);

    this.emitter = this._scene.add.particles(0, 0, 'particle', {
      frequency: -1,
      blendMode: 'ADD',
      alpha: {start: 1, end: 0, ease: 'Linear'},
      lifespan: 1500,
      scale: 0.7,
      speed: 50,
      tint: [tint]
    });
    this.emitter.setPosition(this.x, this.y);
    this.emitter.stop();
    this.emitter.startFollow(this);

  }
}

export default Bullet;
