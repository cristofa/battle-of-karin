import Phaser from 'phaser';

import commonConfig from '../../../common/commonConfig';

class Background {

  constructor(scene, starsNo) {
    this._graphics = scene.add.graphics({ lineStyle: { width: 2, color: 0x00ff00 }});
    this._graphics.beginPath();
    this._graphics.moveTo(0, 0);
    this._graphics.lineTo(0, commonConfig.game.size.width);
    this._graphics.lineTo(commonConfig.game.size.height, commonConfig.game.size.width);
    this._graphics.lineTo(commonConfig.game.size.height, 0);
    this._graphics.lineTo(0, 0);
    this._graphics.closePath();
    this._graphics.strokePath();

    const stars = scene.add.group({ key: 'star', frameQuantity: commonConfig.game.size.width * commonConfig.game.size.height / starsNo });
    const rect = new Phaser.Geom.Rectangle(0, 0,commonConfig.game.size.width, commonConfig.game.size.height);
    Phaser.Actions.RandomRectangle(stars.getChildren(), rect);
  }

}

export default Background;
