import Phaser from 'phaser';

import Preload from './scenes/Preload';
import Boot from './scenes/Boot';
import Lobby from './scenes/Lobby';
import Play from './scenes/Play';

const config = {
  type: Phaser.AUTO,
  scale: {
    mode: Phaser.Scale.RESIZE,
    parent: 'game',
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: '100%',
    height: '100%',
  },
  scene: [Boot, Preload, Lobby, Play ],
  transparent: false
};

export default config;



