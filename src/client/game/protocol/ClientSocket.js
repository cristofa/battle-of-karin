import commonConfig from '../../../common/commonConfig';
import NET_MSGS from '../../../common/NET_MSGS';

class ClientSocket  {
  constructor(gameHostIP, namespace, playerId) {
    this._playerId = playerId;
    const server = `${location.protocol == 'https:' ? 'wss:' : 'ws:'}//${gameHostIP}:${commonConfig.wsPort}/${namespace}`;
    const connectionOptions = {
      'force new connection': true,
      'reconnectionAttempts': 'Infinity',
      'timeout': 10000,
      'transports': ['websocket']
    };

    // eslint-disable-next-line no-undef
    this.socket = io(server, connectionOptions);
  }

  disconnect() {
    this.socket.disconnect();
  }

  emit(event, args) {
    this.socket.emit(event, args);
  }

  login() {
    this.socket.emit(NET_MSGS.LOGIN, { playerId: this._playerId });
  }

  init(handlers) {

    Object.keys(handlers).forEach(h => {
      this.socket.on(h, (args) => {
        if (h == NET_MSGS.CONNECT) {
          handlers[h](this.socket.id);
        } else {
          handlers[h](args);
        }
      });
    });

  }

}


export default ClientSocket;
