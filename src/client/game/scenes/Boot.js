import Phaser from 'phaser';

class Boot extends Phaser.Scene {
  constructor() {
    super({key: 'Boot'});
  }

  preload() {
    this.load.image('logo', 'assets/sprites/logo.png');
  }

  create() {
    this.cameras.resize(window.innerWidth, window.innerHeight);
    this.cameras.main.backgroundColor.setFromRGB({r: 30,g: 30, b: 30,a: 255});
    this.scene.start('Preload');
  }

}

export default Boot;
