import Phaser from 'phaser';

import ClientPlayer from '../model/ClientPlayer';
import {EVENTS } from './EVENTS';
import eventsEmitter from './EVENTS';
import Background from '../model/Background';

class Lobby extends Phaser.Scene {
  constructor() {
    super({key: 'Lobby'});
    eventsEmitter.on(EVENTS.GO_TO_LOBBY, this.initLobby, this);
  }

  preload() {
    this.cameras.resize(window.innerWidth, window.innerHeight);
    this.scale.on('resize', this.resize, this);
  }

  create() {

    this.middleware = () => next => (action) => {
      let bodyColor, cockpitColor, flameColor;

      switch (action.type) {
      case 'messaging/playGame':
        this.game.gameHostIP = action.payload.gameHostIP;
        this.game.ioNamespace = action.payload.namespace;
        this.game.playerId = action.payload.playerId;
        this.scene.switch('Play');
        eventsEmitter.emit(EVENTS.PLAY);
        break;
      case 'messaging/changeColors':
        ({bodyColor, cockpitColor, flameColor} = action.payload);
        this.player.updateTexture(bodyColor, cockpitColor);
        this.player.updateEmitters(flameColor);
        break;
      case 'game/showCustomize':
        return next(action);
      default:
        return next(action);
      }
    };

    this.game.middlewares.addMiddleware(this.middleware);
    this.cameras.main.backgroundColor.setFromRGB({r: 0,g: 0, b: 0,a: 255});

    this.game.overlay.setUIVisible(true);
    this.bg = new Background(this, 5000);

    this.player = new ClientPlayer(this, this.cameras.main.width/3, this.cameras.main.height/3, '100', 'name', 1);

  }

  emitSoundIfVisible() {
  }

  resize (gameSize) {
    this.cameras.resize(gameSize.width, gameSize.height);
    this.player.x = this.cameras.main.width/3;
    this.player.y = this.cameras.main.height/3;
  }

  initLobby() {
  }

}

export default Lobby;
