import Phaser from 'phaser';

import Radar from '../model/Radar';

class HUD extends Phaser.Scene {
  constructor() {
    super({key: 'HUD'});
  }

  create() {
    this.radar = new Radar(this, 0, 0);
    this.add.existing(this.radar);
    this._mainCamera = this.cameras.main;

    this.resize();

    this.scale.on('resize', this.resize, this);

  }

  updateRadar(players) {
    this.radar.updatePlayers(players);
  }

  resize() {
    this.radar.x = this._mainCamera.width - this.radar.width;
    this.radar.y = this._mainCamera.height - this.radar.height/2 - this._mainCamera.height/2;
  }
}

export default HUD;
