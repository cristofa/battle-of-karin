import Phaser from 'phaser';
import msgpack from 'msgpack-lite';

import ClientSocket from '../protocol/ClientSocket';
import ClientPlayer from '../model/ClientPlayer';
import Background from '../model/Background';
import NET_MSGS from '../../../common/NET_MSGS';
import commonConfig from '../../../common/commonConfig';
import Meteor from '../model/Meteor';
import sounds from '../model/Sounds';
import {EVENTS } from './EVENTS';
import eventsEmitter from './EVENTS';
import consoleMsg from '../../../common/consoleMsg';
import Sounds from '../model/Sounds';
import HUD from './HUD';
import {ERROR} from '../../../common/constants';

class Play extends Phaser.Scene {

  _myId;
  _currentPlayer;
  _deathScreen;
  _instructionsVisible;
  _time;
  _warmup;
  _musicPlaying;

  constructor() {
    super({key: 'Play' });
    eventsEmitter.on(EVENTS.PLAY, this.joinGame, this);
  }

  removePlayer(playerId) {
    this.game.overlay.removePlayer(playerId);

    this.players.getChildren().forEach((player) => {
      if (playerId === player.playerId) {
        player.destroy();
        delete this.playersMap[player.id];
      }
    });
  }

  displayPlayer(playerInfo) {
    const player = new ClientPlayer(this, playerInfo.x, playerInfo.y, playerInfo.playerId, playerInfo.playerName, playerInfo.id);

    if (playerInfo.bodyColor) {
      player.updateTexture(playerInfo.bodyColor, playerInfo.cockpitColor);
      player.updateEmitters(playerInfo.flameColor);
    }

    player.stopEngines();
    this.players.add(player);
    this.playersMap[playerInfo.id] = player;
    if (playerInfo.playerId === this._myId) {
      player.self = true;
      this._currentPlayer = player;
      this.cameras.main.startFollow(player);
    }
  }

  initPlayers(players) {
    Object.keys(players).forEach((id) => {
      this.displayPlayer(players[id]);
    });
  }

  updatePlayers(updateMSG) {
    const bufView = new Uint8Array(updateMSG);
    const update = msgpack.decode(bufView);

    if (update.length < 6) {
      return;
    }

    let index = 0;
    do {
      const player = this.playersMap[update[index++]];
      if (!player) {
        return;
      }
      //[info.id, info.x, info.y, info.rotation, info.protection, info.engines, info.life];
      player.setPosition(update[index++], update[index++]);
      player.setAngle(update[index++]);
      player.setProtection(update[index++]);
      player.setEngines(update[index++]);
      player.setLife(update[index++]);
      player.setPowered(update[index++]);
      const bullets = {};
      while(update[index] !== Number.MIN_SAFE_INTEGER) {
        const bullet = {};
        //[b.id, b.x, b.y, b.active, b.armed]
        const id = update[index++];
        bullet.x = update[index++];
        bullet.y = update[index++];
        bullet.active = update[index++];
        bullet.armed = update[index++];
        bullets[id] = bullet;
      }
      player.setBullets(bullets);
      index++;
    } while (update[index] !== Number.MAX_SAFE_INTEGER);

    index++;
    const meteors = {};
    while(update[index] != Number.MAX_SAFE_INTEGER) {
      const meteor = {};
      meteor.id = update[index++];
      meteor.x = update[index++];
      meteor.y = update[index++];
      meteor.fraction = update[index++];
      meteors[meteor.id] = meteor;
    }

    const emitSounds = this.meteors.getChildren().length > 0;
    const found = [];
    this.meteors.getChildren().forEach((meteor) => {
      const info = meteors[meteor.id];
      if (info) {
        found.push(meteor.id);
        meteor.setPosition(info.x, info.y);
        meteor.fraction = info.fraction;
      } else {
        meteor.destroy();
      }
    });
    if ( Object.keys(meteors).length > this.meteors.getChildren().length) {
      const newMeteors = Object.keys(meteors).filter(m => found.indexOf(parseInt(m)) < 0);
      newMeteors.forEach((id) => {
        const info = meteors[id];
        const meteor = new Meteor(this, info.x, info.y, info.id);
        meteor.fraction = info.fraction;
        this.meteors.add(meteor);
        if (meteor.fraction < 5 && emitSounds) {
          this.emitSoundIfVisible(meteor.x, meteor.y, Sounds.METEOR);
        }
      });
    }

    this.hud.updateRadar(this.players.getChildren());
  }

  create() {

    this.cameras.main.setBounds(0, 0, commonConfig.game.size.width, commonConfig.game.size.height);

    this.middleware = () => next => (action) => {
      switch (action.type) {
      case 'messaging/sendVictimMessage':
        this.socket.emit(NET_MSGS.PLAYER.MSG_TO_KILLER, action.payload);
        break;
      case 'messaging/sendWarmupMessage':
        this.socket.emit(NET_MSGS.PLAYER.WARMUP_MSG, action.payload);
        break;
      case 'messaging/toggleMuteMusic':
        this._musicMuted = !this._musicMuted;
        if (this._musicMuted) {
          this.audioManager.stopByKey(sounds.BACKGROUND);
          this._musicPlaying = false;
        }
        break;
      case 'messaging/goToLobby':
        this.socket.disconnect();
        this.players.clear(true, true);
        this.meteors.clear(true, true);
        this.railgunShots.clear(true, true);
        this.audioManager.stopAll();
        this._musicPlaying = false;
        eventsEmitter.emit(EVENTS.GO_TO_LOBBY);
        this.scene.switch('Lobby');
        break;
      default:
        return next(action);
      }
    };
    this.game.middlewares.addMiddleware(this.middleware);

    this._instructionsVisible = false;
    this._time = 0;
    this._lastShotSound = new Date();
    this._warmup = false;
    this._musicPlaying = false;
    this._musicMuted = window.location != window.parent.location ? false : localStorage.getItem('musicMuted') == 'true';

    //this.cameras.main.backgroundColor.setFromRGB({r: 0,g: 0, b: 0,a: 255});
    this.players = this.add.group();
    this.playersMap = {};
    this.meteors = this.add.group();
    this.railgunShots = this.add.group();


    this.cursors = this.input.keyboard.createCursorKeys();
    this.leftKeyPressed = false;
    this.rightKeyPressed = false;
    this.upKeyPressed = false;
    this.downKeyPressed = false;
    this.spacePressed = false;

    this.keySpace = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
    this.keyX = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.X);
    this.keyC = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.C);
    this.keyOne = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ONE);
    this.keyTwo = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.TWO);
    this.keyThree = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.THREE);
    this.keyNine = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.NINE);
    this.bg = new Background(this, 15000);

    this.hud = this.scene.add('HUD', HUD, true, { x: 0, y: 0 });

    this.anims.create({
      key: 'rotate',
      frames: this.anims.generateFrameNumbers('meteor'),
      frameRate: 20
    });

    this.audioManager = new Phaser.Sound.WebAudioSoundManager(this.game);
    this.audioManager.pauseOnBlur = false;
    Object.values(sounds).forEach(s => {
      this.sound.add(s);
      this.audioManager.add(s);
    });

    window.addEventListener('focus', () => { this.audioManager.mute = false;});
    window.addEventListener('blur', () => { this.audioManager.mute = true;});
  }

  joinGame() {
    this.socket = new ClientSocket(this.game.gameHostIP, this.game.ioNamespace, this.game.playerId);
    this.socket.init(this.createHandlers());
    this.playersMap = {};
    this.clearElements();
    this.game.overlay.showDeathScreen(null);
  }

  clearElements() {
    if (this.players) {
      this.players.clear(true, true);
    }
    if (this.meteors) {
      this.meteors.clear(true, true);
    }
  }

  emitSoundIfVisible(x, y, sound) {
    if (this.cameras.main.worldView.contains(x, y)) {
      if (sound == Sounds.SHOT) {
        const now = new Date();
        if (now - this._lastShotSound < 100) {
          return;
        } else {
          this._lastShotSound = now;
        }
      }
      this.audioManager.play(sound);
    }
  }

  connection(id) {
    this.clearElements();
    this._myId = id;
    this.game.overlay.setSocketId(id);
    this.socket.login();
  }

  updateRanking(players) {
    this.game.overlay.updatePlayers(players);
    this.game.overlay.showHUD(true);
    this.game.overlay.setLoading(false);
  }

  railgunShot(payload) {
    const railgun = this.add.sprite(payload[1], payload[2], 'railgun');
    this.railgunShots.add(railgun);
    railgun.setOrigin(0, 0.5);
    railgun.setScale(0.8, 1);
    railgun.setRotation(payload[3]);
    const player = (this.playersMap[payload[0]]);
    railgun.setTint(player.tint);
    this.tweens.add({
      targets: railgun,
      duration: 500,
      alpha: '0',
      ease: 'Linear',
      onComplete: () => {
        railgun.destroy();
      }
    });
    this.emitSoundIfVisible(player.x, player.y, Sounds.RAILGUN);
  }

  playerAccelerate(payload) {
    const player = this.playersMap[payload];
    player.accelerate();
  }

  playerKill(payload) {
    this.game.overlay.updatePlayer(payload);
    this.game.overlay.showKill(payload);
    if (payload.killer == this._myId && commonConfig.game.streaks.indexOf(payload.killerStreak) >= 0) {
      setTimeout(() =>
      {this.audioManager.play(`threshold${commonConfig.game.streaks.indexOf(payload.killerStreak)}`);}, 500);
    }
    if (payload.killer == this._myId && payload.killerStreak == commonConfig.game.streaks[2]) {
      this._currentPlayer.setBerserk(true);
    } else if (payload.victim == this._myId) {
      setTimeout(() => {
        this._deathScreen = {killerName: payload.killerName, killerId: payload.killer, victimName: payload.victimName};
        this.game.overlay.showDeathScreen(this._deathScreen);
      }, 2000);
      if (payload.killerStreak == commonConfig.game.streaks[2]) {
        this.players.getChildren().forEach((player) => {
          if (payload.killer === player.playerId) {
            player.setBerserk(true);
          }
        });
      }
    }
  }

  msgFromVictim(msg) {
    this.game.overlay.showVictimMsg(msg);
  }

  consoleMsg(msg) {
    this.game.overlay.showConsoleMsg(msg);
  }

  showWinners(msg) {
    this.players.getChildren().forEach((player) => {
      if (msg.indexOf(player.playerId) >= 0) {
        player.setBerserk(true);
      }
    });
  }

  changeGame(msg) {
    this.game.overlay.showConsoleMsg({type: consoleMsg.CHANGING_GAME});
    this.game.overlay.setLoading(true);
    this.socket.disconnect();
    this.game.ioNamespace = msg.namespace;
    this.joinGame();
  }

  setTime(time) {
    this._time = time;
    this.game.overlay.setTime(time);
    if (this._currentPlayer && time == 1) {
      this._currentPlayer.sentMessage = false;
    }
    if (time == commonConfig.game.times.round) {
      //this.audioManager.play(sounds.INTRO);
      this.players.getChildren().forEach((player) => {
        player.reset();
      });
    }

    if (time > commonConfig.game.times.round) {
      if (time - commonConfig.game.times.round < 5) {
        this.audioManager.play(Sounds.COUNTDOWN);
        this._musicPlaying = false;
      }
      if (time == commonConfig.game.times.round) {
        this.audioManager.play(Sounds.BACKGROUND);
        this._musicPlaying = true;
      }
      this._warmup = true;
    } else {
      if (!this._musicPlaying && !this._musicMuted) {
        this.audioManager.play(Sounds.BACKGROUND, {
          seek: Math.abs(time - commonConfig.game.times.round),
          volume: 0.2
        });
        this._musicPlaying = true;
      }
      this._warmup = false;
    }
  }

  showError() {
    this.game.overlay.showError(ERROR.general);
  }

  showKickedOut() {
    this.game.overlay.showError(ERROR.kickedOut);
  }

  createHandlers() {
    const handlers = [];
    handlers[NET_MSGS.ERROR] = this.showError.bind(this);
    handlers[NET_MSGS.KICKED_OUT] = this.showKickedOut.bind(this);
    handlers[NET_MSGS.DISCONNECT] = this.removePlayer.bind(this);
    handlers[NET_MSGS.CONNECT] = this.connection.bind(this);
    handlers[NET_MSGS.TIME] = this.setTime.bind(this);
    handlers[NET_MSGS.WINNERS] = this.showWinners.bind(this);
    handlers[NET_MSGS.CONSOLE] = this.consoleMsg.bind(this);
    handlers[NET_MSGS.PLAYER.INIT] = this.initPlayers.bind(this);
    handlers[NET_MSGS.PLAYER.UPDATE] = this.updatePlayers.bind(this);
    handlers[NET_MSGS.PLAYER.NEW] = this.displayPlayer.bind(this);
    handlers[NET_MSGS.PLAYER.RANKING] = this.updateRanking.bind(this);
    handlers[NET_MSGS.PLAYER.KILL] = this.playerKill.bind(this);
    handlers[NET_MSGS.PLAYER.ACCELERATE] = this.playerAccelerate.bind(this);
    handlers[NET_MSGS.PLAYER.RAILGUN_SHOT] = this.railgunShot.bind(this);
    handlers[NET_MSGS.PLAYER.MSG_FROM_VICTIM] = this.msgFromVictim.bind(this);
    handlers[NET_MSGS.PLAYER.CHANGE_GAME] = this.changeGame.bind(this);
    return handlers;
  }

  showInstructions() {
    this.game.overlay.showInstructions(this._currentPlayer.protection);
  }

  sendMessage(warmup) {
    const onePressed = Phaser.Input.Keyboard.JustDown(this.keyOne);
    const twoPressed = Phaser.Input.Keyboard.JustDown(this.keyTwo);
    const threePressed = warmup ? false : Phaser.Input.Keyboard.JustDown(this.keyThree);
    const ninePressed = warmup ? false : Phaser.Input.Keyboard.JustDown(this.keyNine);
    if (onePressed || twoPressed || threePressed || ninePressed) {
      let msgId;
      if (onePressed) {
        msgId = 0;
      } else if (twoPressed) {
        msgId = 1;
      } else if (threePressed) {
        msgId = 2;
      } else {
        msgId = 9;
      }

      if (warmup) {
        this._currentPlayer.sentMessage = true;
        this.game.overlay.showWarmupMsg(false);
        this.socket.emit(NET_MSGS.PLAYER.WARMUP_MSG, {
          id: msgId,
          sender: this._currentPlayer.playerName
        });
      } else {
        if (this._deathScreen != null) {
          this._currentPlayer.sentMessage = true;
          this.socket.emit(NET_MSGS.PLAYER.MSG_TO_KILLER, {
            id: msgId,
            victimName: this._deathScreen.victimName,
            killerId: this._deathScreen.killerId
          });
          this.game.overlay.markMessage(msgId);
          setTimeout(() => {
            this.game.overlay.showDeathScreen(null);
            this._deathScreen = null;
          }, 500);
        }
      }
    }
  }

  update() {
    if (!this._currentPlayer) {
      return;
    }

    if (!this._currentPlayer.dead) {

      if (this._currentPlayer.protection !== this._instructionsVisible) {
        setTimeout(this.showInstructions.bind(this), 500);
        this._instructionsVisible = this._currentPlayer.protection;
      }

      const left = this.leftKeyPressed;
      const right = this.rightKeyPressed;
      const up = this.upKeyPressed;
      const down = this.downKeyPressed;
      const space = this.spacePressed;
      const Xkey = this.xPressed;
      const Ckey = this.cPressed;

      if (this.cursors.left.isDown) {
        this.leftKeyPressed = true;
        this.rightKeyPressed = false;
      } else if (this.cursors.right.isDown) {
        this.rightKeyPressed = true;
        this.leftKeyPressed = false;
      } else {
        this.leftKeyPressed = false;
        this.rightKeyPressed = false;
      }

      this.upKeyPressed = this.cursors.up.isDown;
      this.downKeyPressed = this.cursors.down.isDown;
      this.spacePressed = this.input.keyboard.checkDown(this.keySpace, 120);
      this.xPressed = Phaser.Input.Keyboard.JustDown(this.keyX);
      this.cPressed = Phaser.Input.Keyboard.JustDown(this.keyC);

      if (left !== this.leftKeyPressed || right !== this.rightKeyPressed ||
        up !== this.upKeyPressed || down !== this.downKeyPressed || space !== this.spacePressed ||
      Xkey !== this.xPressed || Ckey !== this.cPressed) {

        this.socket.emit(NET_MSGS.PLAYER.INPUT, msgpack.encode([
          this.leftKeyPressed ? 1 : 0,
          this.rightKeyPressed ? 1 : 0,
          this.upKeyPressed ? 1 : 0,
          this.downKeyPressed ? 1 : 0,
          this.spacePressed ? 1 : 0,
          this.xPressed ? 1 : 0,
          this.cPressed ? 1 : 0
        ]));
      }
      if (this._warmup && !this._currentPlayer.sentMessage) {
        this.sendMessage(true);
      }
    } else if (!this._currentPlayer.sentMessage || (this._warmup && !this._currentPlayer.sentMessage)) {
      this.sendMessage(this._warmup);
    }
    //console.log(this.game.loop.actualFps);

  }

}

export default Play;
