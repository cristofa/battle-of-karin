import Phaser from 'phaser';

const eventsEmitter = new Phaser.Events.EventEmitter();

const EVENTS =  {
  GO_TO_LOBBY: 'goToLobby',
  PLAY: 'play'
};

export {EVENTS};

export default eventsEmitter;
