import Phaser from 'phaser';

import ProgressBar from '../utils/ProgressBar';


class Preload extends Phaser.Scene {
  constructor() {
    super({key: 'Preload'});
  }

  preload() {
    this.scale.on('resize', this.resize, this);
    this.cameras.resize(window.innerWidth, window.innerHeight);
    this.progressBar = new ProgressBar(this, this.cameras.main.width/2, this.cameras.main.height/2);
    this.add.existing(this.progressBar);

    this.cameras.main.backgroundColor.setFromRGB({r: 0,g: 0, b: 0,a: 255});

    //load all assets here
    this.load.on('progress', (value) => {
      console.log('Progress: ' + value);
      this.progressBar.progress(value);

    });

    this.load.on('complete', () => {
      this.scene.switch('Lobby');
    });

    this.load.spritesheet('player', 'assets/sprites/player.png', { frameWidth: 141, frameHeight: 127, endFrame: 3 });
    this.load.image('player2', 'assets/sprites/player2.png');
    this.load.image('cockpit', 'assets/sprites/cockpit.png');
    this.load.image('cockpit2', 'assets/sprites/cockpit2.png');
    this.load.image('particle', 'assets/sprites/particle.png');
    this.load.image('railgun', 'assets/sprites/railgun.png');
    this.load.image('star', 'assets/sprites/star.png');
    this.load.image('tile', 'assets/sprites/tile.png');
    this.load.image('ship-part', 'assets/sprites/ship-part.png');
    this.load.spritesheet('meteor', 'assets/sprites/meteorsheet.png', { frameWidth: 128, frameHeight: 96, endFrame: 59 });

    this.load.audio('shot', 'assets/audio/shot1.mp3');
    this.load.audio('explode', 'assets/audio/explode.mp3');
    this.load.audio('hit', 'assets/audio/hit.mp3');
    this.load.audio('accel', 'assets/audio/accel.mp3');
    this.load.audio('powered', 'assets/audio/powered.mp3');
    this.load.audio('railgun', 'assets/audio/railgun.mp3');
    this.load.audio('meteor', 'assets/audio/meteor.mp3');
    this.load.audio('bullet', 'assets/audio/bullet.mp3');
    this.load.audio('intro', 'assets/audio/intro.mp3');
    this.load.audio('countdown', 'assets/audio/countdown.mp3');
    this.load.audio('threshold0', 'assets/audio/threshold0.mp3');
    this.load.audio('threshold1', 'assets/audio/threshold1.mp3');
    this.load.audio('threshold2', 'assets/audio/threshold2.mp3');
    this.load.audio('threshold3', 'assets/audio/threshold3.mp3');
    this.load.audio('background', 'assets/audio/background.mp3');
  }

  resize (gameSize) {
    this.cameras.resize(gameSize.width, gameSize.height);
    this.progressBar.x = this.cameras.main.width/2;
    this.progressBar.y = this.cameras.main.height/2;
  }
}

export default Preload;
