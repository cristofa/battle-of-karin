import commonConfig from '../common/commonConfig';

const getPort = (appendClientPort) => {
  if (appendClientPort) {
    return location.port.length > 0 ? `:${location.port}` : '';
  }
  return location.port.length > 0 ? `:${commonConfig.port}` : '';
};

const getWsPort = () => {

  return location.port.length > 0 ? `:${commonConfig.wsPort}` : '';
};


const getHost = (appendClientPort) => {
  return `${location.protocol}//${location.hostname}${getPort(appendClientPort)}`;
};

const getGameHost = (gameHostIP) => {
  return `${location.protocol}//${gameHostIP || commonConfig.gameHosts[0].host}:${commonConfig.gameApiPort}`;
};

const getEmitterConfig = (tint, angle = 180, scale = 0.5) => {
  return {
    frequency: 0,
    maxParticles: 100,
    blendMode: 'ADD',
    alpha: {start: 1, end: 0, ease: 'Linear'},
    angle: angle,
    lifespan: 1500,
    maxVelocityX: 10000,
    maxVelocityY: 10000,
    scale: scale,
    speed: 30,
    tint: [tint]
  };
};
export { getHost, getGameHost, getWsPort, getEmitterConfig };
