import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import {loadableReady} from '@loadable/component';
import {ChakraProvider} from '@chakra-ui/react';
import { BrowserRouter, Route } from 'react-router-dom';
import loadable from '@loadable/component';

import App from './app/App';
import './app/i18n';
// eslint-disable-next-line import/order
import theme from './app/theme';

const AfterLogin = loadable(() => import('./components/AfterLogin'));

import commonConfig from '../common/commonConfig';

function Main() {
  return (
    <ChakraProvider theme={theme}>
      <Suspense fallback={null}>
        <BrowserRouter>
          <Route path='/' component={App} />
          {/* eslint-disable-next-line no-undef */}
          <Route path={commonConfig.loginRedirect} component={AfterLogin} />
        </BrowserRouter>
      </Suspense>
    </ChakraProvider>
  );
}

loadableReady(() => {
  ReactDOM.render(<Main/>, document.querySelector('#root'));
});
