import React from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';

import store from './store';
import Game from '../components/Game';
import Overlay from '../components/Overlay';
import ModalDialog from '../components/ModalDialog';

const App = () =>
  <Provider store={store}>
    <ModalDialog/>
    <Game/>
    <Overlay/>
  </Provider>;

export default hot(App);
