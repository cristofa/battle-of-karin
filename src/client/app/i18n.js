import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';

const languages = [{key: 'en', label: 'EN'},{key: 'es', label: 'ES'}, {key: 'pl', label: 'PL'}];

i18n
  .use(Backend)
  // connect with React
  .use(initReactI18next)
  .init({
    lng: window.location != window.parent.location  ? 'en' : (localStorage.getItem('language') || 'en'),
    fallbackLng: 'en',
    whitelist: languages.map(l => l.key),
    interpolation: {
      escapeValue: false
    },
  });
export { languages };

export default i18n;
