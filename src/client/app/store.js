import { configureStore } from '@reduxjs/toolkit';

import playersReducer from './slices/playersSlice';
import messagingReducer from './slices/messagingSlice';
import gameSlice from './slices/gameSlice';
import gameMiddlewares from './gameMiddlewares';

export default configureStore({
  reducer: {
    players: playersReducer,
    messaging: messagingReducer,
    game: gameSlice
  },
  middleware: [gameMiddlewares.enhancer]
});
