import { extendTheme } from '@chakra-ui/react';

const colors = {
  brand:
    {
      50: '#e0ffe5',
      100: '#b5fac0',
      200: '#87f49a',
      300: '#5af073',
      400: '#2dec4c',
      500: '#13d233',
      600: '#09a327',
      700: '#02751a',
      800: '#00470d',
      900: '#001900',
    },
};

const theme = extendTheme({ colors });

export default theme;
