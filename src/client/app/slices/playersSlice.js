import { createSlice, createEntityAdapter, createSelector } from '@reduxjs/toolkit';

const playersAdapter = createEntityAdapter({
  selectId: (player) => player.id
});

const initialState = playersAdapter.getInitialState({
  ids: [],
  entities: {
  },
});

export const playersSlice = createSlice({
  name: 'players',
  initialState,
  reducers: {
    updatePlayers: (state, action) => {
      playersAdapter.setAll(state, action.payload);
    },
    updatePlayer: (state, action) => {
      playersAdapter.updateOne(state,
        {id: action.payload.killer, changes: {points: action.payload.killerPoints}});
    },
    removePlayer: (state, action) => {
      playersAdapter.removeOne(state, action.payload);
    }
  },
});

export const { updatePlayers, updatePlayer, removePlayer } = playersSlice.actions;

export const {
  selectAll: selectAllPlayers,
  selectById: selectPlayerById,
  selectIds: selectPlayersIds,
} = playersAdapter.getSelectors(state => state.players);

export const getSorted = createSelector(
  selectAllPlayers,
  (players) => {
    players.sort((a, b) => b.points - a.points);
    return players;
  });

export const getWinner = createSelector(
  selectAllPlayers,
  (players) => {
    if (players.length == 0) {
      return [''];
    }
    let maxPoints = 0;
    players.forEach(p => {
      if (p.points > maxPoints) {
        maxPoints = p.points;
      }
    });
    return players.filter(p => p.points == maxPoints && p.points > 0).map(p => p.name);
  });

export const getRankingEntries = createSelector(
  selectAllPlayers,
  (_, id) => id,
  (players, id) => {
    if (players.length < 3) {
      return [];
    }
    players.sort((a, b) => b.points - a.points);
    let score = players[0].points + 1;
    let rank = 0;
    const ranks = {};
    players.forEach(p => {
      if (p.points != score) {
        rank++;
      }
      ranks[p.id] = rank;
      score = p.points;
    });
    const currentIndex = players.findIndex(p => p.id === id);

    if (currentIndex < 0) {
      return [];
    }

    let subArray;
    const diff = [];
    if (currentIndex == 0) {
      subArray = players.slice(0, 3);
      diff[0] = null;
      diff[1] = subArray[1].points - subArray[0].points;
      diff[2] = subArray[2].points - subArray[0].points;
    } else if (currentIndex == players.length - 1) {
      subArray = players.slice(players.length-3, players.length);
      diff[2] = null;
      diff[0] = subArray[0].points - subArray[2].points;
      diff[1] = subArray[1].points - subArray[2].points;
    } else {
      subArray = players.slice(currentIndex - 1, currentIndex + 2);
      diff[1] = null;
      diff[0] = subArray[0].points - subArray[1].points;
      diff[2] = subArray[2].points - subArray[1].points;
    }
    const rankedPlayers = subArray.map((p,i) => { return {...p, diff: diff[i], rank: ranks[p.id], self: p.id == id};});

    return rankedPlayers;
  }
);
export default playersSlice.reducer;
