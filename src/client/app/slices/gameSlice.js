import { createSlice } from '@reduxjs/toolkit';

import commonConfig from '../../../common/commonConfig';

const getBoolean = (value) => {
  if (value && value == 'true') {
    return true;
  }
  return false;
};

const getDefaultHost = () => {
  if (window.location != window.parent.location || typeof localStorage == 'undefined') {
    return null;
  }
  const urlParams = new URLSearchParams(window.location.search);
  const server = urlParams.get('server');
  if (server) {
    return server;
  }
  return localStorage.getItem('gameHostIP');
};

const initialState ={
  uiVisible: false,
  showingHUD: false,
  player: {},
  instructionsVisible: false,
  customize: false,
  isLoading: false,
  socketId: null,
  namespace: null,
  error: null,
  kill: null,
  consoleMsg: null,
  victimMsg: null,
  deathScreen: null,
  warmup: false,
  showWarmupMsg: false,
  time: 200,
  firstGame: true,
  musicMuted: (window.location != window.parent.location || typeof localStorage == 'undefined') ? false : getBoolean(localStorage.getItem('musicMuted')),
  gameHostIP: getDefaultHost() || commonConfig.gameHosts[0].host
};

export const gameSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    setUIVisible: (state, action) => {
      state.uiVisible = action.payload;
    },
    showHUD: (state, action) => {
      state.showingHUD = action.payload;
    },
    showInstructions: (state, action) => {
      state.instructionsVisible = action.payload;
    },
    setPlayerDetails: (state, action) => {
      state.player = action.payload;
    },
    showCustomize: (state, action) => {
      state.customize = action.payload;
    },
    setLoading: (state, action) => {
      state.isLoading = action.payload;
    },
    showError: (state, action) => {
      state.error = action.payload;
    },
    setSocketId: (state, action) => {
      state.socketId = action.payload;
    },
    setNamespace: (state, action) => {
      state.namespace = action.payload.namespace;
    },
    toggleMusicMuted: (state) => {
      state.musicMuted = !state.musicMuted;
      if (window.location == window.parent.location) {
        localStorage.setItem('musicMuted', state.musicMuted);
      }
    },
    setGameHost: (state, action) => {
      state.gameHostIP = action.payload.gameHostIP;
      if (window.location == window.parent.location) {
        localStorage.setItem('gameHostIP', state.gameHostIP);
      }
    },
    markMessage: (state, action) => {
      if (state.deathScreen) {
        state.deathScreen.marked = action.payload;
      }
    },
    showDeathScreen: (state, action) => {
      state.deathScreen = action.payload;
    },
    showWarmupMsg: (state,action) => {
      state.showWarmupMsg = action.payload;

    },
    showVictimMsg: (state, action) => {
      state.victimMsg = action.payload;
    },
    setTime: (state, action) => {
      state.time = action.payload;
      const warmUp = action.payload > commonConfig.game.times.round;
      if (!state.warmup && warmUp) {
        state.showWarmupMsg = true;
      }
      if (warmUp) {
        state.deathScreen = null;
      }
      if (state.firstGame && !warmUp) {
        state.firstGame = false;
      }
      state.warmup = warmUp;

    },
    showConsoleMsg: (state, action) => {
      state.consoleMsg = action.payload;
    },
    showKill: (state, action) => {
      if (action.payload) {
        state.kill = {
          weapon: action.payload.weapon,
          killer: action.payload.killer,
          killerName: action.payload.killerName,
          killerStreak: action.payload.killerStreak,
          victim: action.payload.victim,
          victimName: action.payload.victimName,
          firstBlood: action.payload.firstBlood || false
        };
      } else {
        state.kill = null;
      }
    }
  },
});

export const {
  setUIVisible,
  showHUD,
  setPlayerDetails,
  showCustomize,
  setLoading,
  showError,
  setSocketId,
  setNamespace,
  setGameHost,
  showKill,
  markMessage,
  showVictimMsg,
  showDeathScreen,
  setTime,
  showWarmupMsg,
  showConsoleMsg,
  goToLobby,
  toggleMusicMuted,
  showInstructions
} = gameSlice.actions;


export default gameSlice.reducer;
