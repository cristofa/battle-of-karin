import { createSlice } from '@reduxjs/toolkit';

const initialState = { host: '', connected: null,  error: ''};


const messaging = createSlice({
  name: 'messaging',
  initialState,
  reducers: {
    playGame: () => {
    },
    goToLobby: () => {
    },
    changeColors: () => {
    },
    sendVictimMessage: () => {
    },
    sendWarmupMessage: () => {
    },
    toggleMuteMusic: () => {
    }
  }
});

export const {
  playGame, changeColors, sendVictimMessage,sendWarmupMessage, goToLobby, toggleMuteMusic
} = messaging.actions;


export default messaging.reducer;
