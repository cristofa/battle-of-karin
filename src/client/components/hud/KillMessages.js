import React, {useEffect} from 'react';
import {Box, createStandaloneToast, Text, Flex, Image} from '@chakra-ui/react';
import {useSelector} from 'react-redux';

const getBgColor = (socketId, killerId, victimId) => {
  if (socketId == killerId) {
    return 'rgba(0, 255, 0, 0.1)';
  } else if (socketId == victimId) {
    return 'rgba(255, 0, 0, 0.1)';
  }
  return 'rgba(0, 0, 0, 0.8)';
};

const KillMessages = () => {
  const toast = createStandaloneToast();

  const socketId = useSelector(state => state.game.socketId);
  const kill = useSelector(state => state.game.kill);

  useEffect(() => {
    if (kill) {
      const bgColor = getBgColor(socketId, kill.killer, kill.victim);
      toast({
        // eslint-disable-next-line react/display-name
        render: () => (
          <Box bg={bgColor} w='250px' ml='40px'>
            <Flex direction='row' justify='space-around'>
              <Text fontSize={'md'} color='white' align='center' isTruncated maxWidth='110px'>
                {kill.killerName}
              </Text>
              <Image src={kill.weapon == 0 ? '/assets/images/basic-weapon.png' : '/assets/images/railgun.png'} />
              <Text fontSize={'md'} color='white' align='center'  isTruncated maxWidth='110px'>
                {kill.victimName}
              </Text>
            </Flex>
          </Box>
        ),
        position: 'top-right',
        duration: 4000,
      });
    }
  }, [kill]);


  return (<React.Fragment></React.Fragment>);
};

export default KillMessages;
