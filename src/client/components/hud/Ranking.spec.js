import { describe, expect } from '@jest/globals';
import React from 'react';
import { render, screen } from '@testing-library/react';
import {Provider} from 'react-redux';

import {updatePlayers} from '../../app/slices/playersSlice';
import {setSocketId} from '../../app/slices/gameSlice';
import store from '../../app/store';
import Ranking from './Ranking';
import '../../i18nForTests';

const setPlayers = (players) => {
  store.dispatch(updatePlayers(players));
};

const socketId = 1;

const getPlayers = () => {
  const firstPlayer = {id: 8, points: 3, name: 'FirstPlayer'};
  const secondPlayer = {id: 9, points: 2, name: 'SecondPlayer'};
  const thirdPlayer = {id: 10, points: 1, name: 'ThirdPlayer'};
  const zeroPointsPlayer = {id: 11, points: 0, name: 'OtherPlayer'};
  return [firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer];
};

const setSelfId = () => {
  store.dispatch(setSocketId(socketId));
};

beforeEach(() => {
  setPlayers([]);
  setSelfId();
});

describe('Ranking component', () => {
  test('renders Ranking when player in the middle with distinct points', () => {
    const [firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer] = getPlayers();
    secondPlayer.id = socketId;
    setPlayers([firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer]);

    render(<Provider store={store}><Ranking/></Provider>);

    expect(screen.getByText('1ordinal_st FirstPlayer +1 (3points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('2ordinal_nd SecondPlayer (2points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('3ordinal_rd ThirdPlayer -1 (1points_abbrv)')).toBeInTheDocument();

  });

  test('renders Ranking when player first with distinct points', () => {
    const [firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer] = getPlayers();
    firstPlayer.id = socketId;
    setPlayers([firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer]);
    render(<Provider store={store}><Ranking/></Provider>);

    expect(screen.getByText('1ordinal_st FirstPlayer (3points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('2ordinal_nd SecondPlayer -1 (2points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('3ordinal_rd ThirdPlayer -2 (1points_abbrv)')).toBeInTheDocument();
  });

  test('renders Ranking when player last with distinct points', () => {
    const [firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer] = getPlayers();
    zeroPointsPlayer.id = socketId;
    setPlayers([firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer]);
    render(<Provider store={store}><Ranking/></Provider>);

    expect(screen.getByText('2ordinal_nd SecondPlayer +2 (2points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('3ordinal_rd ThirdPlayer +1 (1points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('4ordinal_th OtherPlayer (0points_abbrv)')).toBeInTheDocument();
  });


  test('renders Ranking when player first with same points', () => {
    const [firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer] = getPlayers();
    firstPlayer.id = socketId;
    firstPlayer.points = 2;
    setPlayers([firstPlayer, secondPlayer, thirdPlayer, zeroPointsPlayer]);
    render(<Provider store={store}><Ranking/></Provider>);

    expect(screen.getByText('1ordinal_st FirstPlayer (2points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('1ordinal_st SecondPlayer = (2points_abbrv)')).toBeInTheDocument();
    expect(screen.getByText('2ordinal_nd ThirdPlayer -1 (1points_abbrv)')).toBeInTheDocument();
  });

});
