import React from 'react';
import {Flex, Box, Center, Text} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import { TimeIcon } from '@chakra-ui/icons';

import commonConfig from '../../../common/commonConfig';
import RankingTable from './RankingTable';
import WarmupScreen from './WarmupScreen';

const formatTime = (time) => {
  const minutes = Math.floor(time/60);
  const seconds = time%60;
  return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
};

const Timer = () => {
  const time = useSelector(state => state.game.time);
  const warmup = useSelector(state => state.game.warmup);

  let displayTime = time;
  if (time > commonConfig.game.times.round) {
    displayTime -= commonConfig.game.times.round;
  }

  return (
    <Flex direction='column' justify='center'>
      <Center>
        {warmup ? null :
          (<Box bg='rgba(0, 0, 0, 0.8)' w='70px' zIndex='10'>
            <Text align='center' fontSize='xl'
              color={displayTime < 10 && !warmup ? 'red' : 'white'}><TimeIcon pb='3px' boxSize='4' mr='5px'/>{formatTime(displayTime)}</Text>
          </Box>)
        }
        {warmup ? <RankingTable visible={warmup}/> : null}
        {warmup ? <WarmupScreen/> : null}
      </Center>
    </Flex>);
};

export default Timer;
