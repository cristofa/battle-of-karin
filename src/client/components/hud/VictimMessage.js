import React, {useEffect} from 'react';
import {Box, useToast, Text, Flex} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';


const VictimMessage = () => {
  const toast = useToast();
  const { t } = useTranslation();

  const victimMsg = useSelector(state => state.game.victimMsg);

  useEffect(() => {
    if (victimMsg) {
      toast({
        // eslint-disable-next-line react/display-name
        render: () => (
          <Box bg='rgba(0, 0, 0, 0.8)' ml='40px'>
            <Flex direction='row' justify='left'>
              <Text pr='10px' fontSize={'xl'} color='green' align='left' isTruncated maxWidth='110px'>
                {victimMsg.victimName}:
              </Text>
              <Text fontSize={'xl'} color='white' align='left'>
                {t(`${victimMsg.warmup ? 'warmupMsg' : 'victimMsg'}${victimMsg.msgId}`)}
              </Text>
            </Flex>
          </Box>
        ),
        position: 'bottom-right',
        duration: 4000,
      });
    }
  }, [victimMsg]);


  return (<React.Fragment></React.Fragment>);
};

export default VictimMessage;
