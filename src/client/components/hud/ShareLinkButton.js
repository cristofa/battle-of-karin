import React, {useState} from 'react';
import {Box, Text, Input} from '@chakra-ui/react';
import {LinkIcon} from '@chakra-ui/icons';
import {useSelector} from 'react-redux';

import {getHost} from '../../utils';

const inputId = 'linkInput';
const getLinkToGame = (gameUUID, server) => {
  return `${getHost()}?uuid=${gameUUID}&server=${server}`;
};
const selectLink = () => {
  document.getElementById(inputId).select();
};

const ShareLinkButton = () => {

  const [linkVisible, showLink] = useState(false);

  const namespace = useSelector(state => state.game.namespace);
  const gameHost = useSelector(state => state.game.gameHostIP);

  return (
    <Box style={{cursor: 'pointer'}}  zIndex='100' bg='rgba(0, 0, 0, 0.8)' paddingLeft='10px'>
      <Text onClick={() => {showLink(!linkVisible);}} color='white'><LinkIcon boxSize='5' /></Text>
      <Input readOnly display={linkVisible ? 'block' : 'none'} value={getLinkToGame(namespace, gameHost)}
        variant='outline' mt='5px' color='blue.200'
        id={inputId} onClick={selectLink}/>
    </Box>);
};

export default ShareLinkButton;
