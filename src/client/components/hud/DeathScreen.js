import React, {useEffect, useState} from 'react';
import {
  Heading, Box, Center, Kbd, Flex, Slide, Text} from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {showDeathScreen} from '../../app/slices/gameSlice';
import commonConfig from '../../../common/commonConfig';
import {sendVictimMessage} from '../../app/slices/messagingSlice';
import RankingTable from './RankingTable';

const DeathScreen = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const time = useSelector(state => state.game.time);
  const [msgNo, setMsgNo] = useState(1);
  const deathScreen = useSelector(state => state.game.deathScreen);

  const [startTime, setStartTime] = useState(null);

  const sendMsg = (index) => {
    dispatch(showDeathScreen(null));
    dispatch(sendVictimMessage({id: index, killerId: deathScreen.killerId, victimName: deathScreen.victimName}));
  };

  useEffect(() => {
    if (deathScreen && !startTime) {
      setStartTime(time);
      setMsgNo(Math.floor(Math.random()*3));
    }
    if (!deathScreen) {
      setStartTime(null);
    }
    if (startTime - time === commonConfig.game.times.respawnTime) {
      setStartTime(null);
      dispatch(showDeathScreen(null));
    }
  }, [time, deathScreen]);

  if (!deathScreen) {
    return null;
  }

  const displayTime = startTime ? commonConfig.game.times.respawnTime - (startTime - time) : 0;
  const name = deathScreen ? deathScreen.killerName : '';
  const messages = deathScreen ? [0,1,2].map(i =>
    <Flex style={{cursor: 'pointer'}} onClick={() => sendMsg(i)} p='5px' pl='10px' pr='10px' direction='column' align='center' key={i}>
      <Kbd>{i+1}</Kbd><Text align='center' fontSize='lg' color={deathScreen.marked == i ? 'red' : 'white'}>{t(`victimMsg${i}`)}</Text>
    </Flex>
  ) : null;

  return (
    <React.Fragment>
      <RankingTable visible={deathScreen}/>
      <Slide in={deathScreen} direction='bottom' style={{zIndex: '10'}}>
        <Center>
          <Box mb='20px' p='10px' pb='5px' bg='rgba(0, 0, 0, 0.8)'>
            <Heading align='center' fontSize='xl' color='white'>{displayTime}</Heading>
            <Heading p='5px' align='center' fontSize='xl' color='red.400'>
              {t(`youGotKilled${msgNo}`, { killer: name})}</Heading>
            <Heading p='5px' align='center' fontSize='lg' color='white'>{t('sendMessage')}
            </Heading>
            <Flex p='10px' direction='row' justify='space-around'>
              {messages}
            </Flex>
            <Heading pt='5px' align='center' fontSize='md' color='white'>{t('pressKey')}</Heading>
          </Box>
        </Center>
      </Slide>
    </React.Fragment>);
};

export default DeathScreen;
