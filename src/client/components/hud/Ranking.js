import React from 'react';
import {Flex, Box, Heading} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {getRankingEntries} from '../../app/slices/playersSlice';

const ordinal = (i, t) => {
  const j = i % 10, k = i % 100;
  if (j == 1 && k != 11) {
    return i + t('ordinal_st');
  } else if (j == 2 && k != 12) {
    return i + t('ordinal_nd');
  } else if (j == 3 && k != 13) {
    return i + t('ordinal_rd');
  }
  return i + t('ordinal_th');
};

const parseDiff = (diff) => {
  let result = '';
  if (diff == null) {
    return result;
  } else if (diff == 0) {
    result = '=';
  } else {
    if (diff > 0) {
      result = '+';
    }
    result += diff;
  }
  return result;
};

const getRankEntry = (entry, t) => {
  return <Heading key={entry.id} pl={entry.self ? '0px' : '15px'} fontSize={entry.self ? 'lg' : 'md'} color={entry.self ? 'white' : 'grey'} align='left'>
    {ordinal(entry.rank, t)} {entry.name} {parseDiff(entry.diff)} ({entry.points}{t('points_abbrv')})
  </Heading>;

};

const Ranking = () => {
  const { t } = useTranslation();

  const socketId = useSelector(state => state.game.socketId);
  const rankingEntries = useSelector((state) => getRankingEntries(state, socketId));

  const renderedEntries = rankingEntries.map(re => getRankEntry(re, t));

  return (
    <Flex direction='column' justify='center' pos='absolute' top='35px' left='15px'  h='10%' opacity='0.9'>
      <Box p='5px' pr='5px' bg='rgba(0, 0, 0, 0.8)'>
        {renderedEntries}
      </Box>
    </Flex>);
};

export default Ranking;
