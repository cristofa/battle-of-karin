import React, {useEffect} from 'react';
import {Box, createStandaloneToast, Heading, Flex} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import commonConfig from '../../../common/commonConfig';

const getStreakMessage = (streak, firstBlood) => {
  if (firstBlood) {
    return 'youDrewFirstBlood';
  }
  return `streak_${commonConfig.game.streaks.indexOf(streak)}`;
};

const getFontSize = (streak) => {
  if (streak == commonConfig.game.streaks[2]) {
    return '2xl';
  } else if (streak == commonConfig.game.streaks[1]) {
    return 'xl';
  }
  return 'lg';
};

const StreakMessage = () => {
  const toast = createStandaloneToast();

  const socketId = useSelector(state => state.game.socketId);
  const kill = useSelector(state => state.game.kill);
  const { t } = useTranslation();

  useEffect(() => {
    if (kill && kill.killer === socketId &&
      (commonConfig.game.streaks.indexOf(kill.killerStreak) > -1 || kill.firstBlood)) {

      const bgColor = kill.killerStreak == commonConfig.game.streaks[2] ?
        'rgba(255, 0, 0, 0.2)' : 'rgba(255, 255, 255, 0.2)';
      toast({
        // eslint-disable-next-line react/display-name
        render: () => (
          <Box bg={bgColor} w='350px'>
            <Flex direction='row' justify='space-around'>
              <Heading fontSize={getFontSize(kill.killerStreak)} color='white' align='center'>
                {t(getStreakMessage(kill.killerStreak, kill.firstBlood))}
              </Heading>
            </Flex>
          </Box>
        ),
        position: 'bottom',
        duration: 4000,
      });
    }
  }, [kill]);


  return (<React.Fragment></React.Fragment>);
};

export default StreakMessage;
