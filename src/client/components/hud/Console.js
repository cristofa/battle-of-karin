import React, {useEffect} from 'react';
import {Container, createStandaloneToast, Text} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import consoleMsg from '../../../common/consoleMsg';

const getBgColor = (msgType) => {
  switch(msgType) {
  case consoleMsg.JOINED:
    return 'rgba(0, 255, 0, 0.1)';
  case consoleMsg.FIRST_BLOOD:
    return 'rgba(255, 0, 0, 0.1)';
  default:
    return 'rgba(0, 0, 0, 0.8)';
  }
};

const getMessage = (message, t) => {
  const params = message.params ? {...message.params} : {};

  if (params.rank !== null) {
    params.rank = t(`rank${params.rank}`);
  }

  return t(message.type, params);
};

const Console = () => {
  const toast = createStandaloneToast();
  const { t } = useTranslation();

  const consoleMsg = useSelector(state => state.game.consoleMsg);

  useEffect(() => {
    if (consoleMsg) {
      const bgColor = getBgColor(consoleMsg.type);
      toast({
        // eslint-disable-next-line react/display-name
        render: () => (
          <Container bg={bgColor} ml='0px' mt='-15px' mb='-15x'>
            <Text fontSize={'sm'} color='white' align='left' style={{whiteSpace: 'nowrap'}}>
              {getMessage(consoleMsg, t)}
            </Text>
          </Container>
        ),
        position: 'bottom-left',
        duration: 4000,
      });
    }
  }, [consoleMsg]);


  return (<React.Fragment></React.Fragment>);
};

export default Console;
