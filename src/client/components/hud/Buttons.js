import React from 'react';
import {Flex} from '@chakra-ui/react';

import BackButton from './BackButton';
import ShareLinkButton from './ShareLinkButton';
import MuteMusicButton from './MuteMusicButton';


const Buttons = () => {

  return (
    <Flex direction='row' position='absolute' left='5px' top='5px'>
      <BackButton/>
      <ShareLinkButton/>
      <MuteMusicButton/>
    </Flex>);
};

export default Buttons;
