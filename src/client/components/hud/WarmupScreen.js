import React from 'react';
import {
  Heading, Box, Center, Kbd, Flex, Slide, Text, Image
} from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import {showWarmupMsg} from '../../app/slices/gameSlice';
import {selectPlayerById, getWinner} from '../../app/slices/playersSlice';
import {sendWarmupMessage} from '../../app/slices/messagingSlice';
import commonConfig from '../../../common/commonConfig';

const WarmupScreen = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const warmup = useSelector(state => state.game.warmup);
  const showMsg = useSelector(state => state.game.showWarmupMsg);
  const firstGame = useSelector(state => state.game.firstGame);
  const currentPlayer = useSelector(state => selectPlayerById(state, state.game.socketId));
  let time = useSelector(state => state.game.time);
  const winner = useSelector(state => getWinner(state));

  const sendMsg = (index) => {
    dispatch(showWarmupMsg(false));
    dispatch(sendWarmupMessage({id: index, sender: currentPlayer.name}));
  };

  const messages = [0,1].map(i =>
    <Flex style={{cursor: 'pointer'}} onClick={() => sendMsg(i)} p='5px' pl='10px' pr='10px' direction='column' align='center' key={i}>
      <Kbd>{i+1}</Kbd><Text align='center' color='white' fontSize='lg'>{t(`warmupMsg${i}`)}</Text>
    </Flex>
  );
  time -= commonConfig.game.times.round;
  const winnerMsg = winner.length > 0 ? (winner.length > 1 ? 'winnersAre' : 'winnerIs') : '';
  const msgVisibility = (showMsg && !firstGame) ? 'block' : 'none';
  const instructionsVisibility = firstGame ? 'block' : 'none';
  return (
    <Slide in={warmup} direction='bottom' style={{zIndex: '10'}}>
      <Center>
        <Box mb='10px' p='5px' bg='rgba(0, 0, 0, 0.8)'>
          <Heading align='center' fontSize='xl' color='white'>{time}</Heading>
          <Heading visibility={winner.length > 0} p='5px' align='center' fontSize='xl' color='green.400'>
            {t(winnerMsg)}</Heading>
          <Heading maxWidth='400px' p='5px' align='center' fontSize='lg' color='blue.500'>{winner.join(', ')}
          </Heading>
          <div style={{display: msgVisibility}}>
            <Flex p='10px' direction='row' justify='space-around'>
              {messages}
            </Flex>
            <Heading align='center' fontSize='md' color='white'>{t('pressKey')}</Heading>
          </div>
          <div style={{display: instructionsVisibility}}>
            <Flex direction='row' align='center'>
              <Image h='80px' src='/assets/images/space.png' alt='space' />
              <Image h='80px' src='/assets/images/arrows.png' alt='Arrows' />
            </Flex>
          </div>
        </Box>
      </Center>
    </Slide>);
};

export default WarmupScreen;
