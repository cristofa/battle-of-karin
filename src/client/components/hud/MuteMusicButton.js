import React from 'react';
import {Box, Text, Image} from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';

import { toggleMusicMuted } from '../../app/slices/gameSlice';
import {toggleMuteMusic} from '../../app/slices/messagingSlice';

const MuteMusicButton = () => {
  const dispatch = useDispatch();
  const musicMuted = useSelector(state => state.game.musicMuted);

  return (
    <Box style={{cursor: 'pointer'}}  zIndex='100' bg='rgba(0, 0, 0, 0.8)' paddingLeft='10px'>
      <Text onClick={() => {dispatch(toggleMusicMuted()); dispatch(toggleMuteMusic());}} color='white'>
        <Image boxSize='30px' src={`/assets/images/${musicMuted ? 'music_muted.png' : 'music_unmuted.png'}`}/>
      </Text>
    </Box>);
};

export default MuteMusicButton;
