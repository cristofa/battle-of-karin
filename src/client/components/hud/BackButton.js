import React from 'react';
import {Box, Text} from '@chakra-ui/react';
import {ChevronLeftIcon} from '@chakra-ui/icons';
import {useDispatch} from 'react-redux';

import {showHUD} from '../../app/slices/gameSlice';
import {goToLobby} from '../../app/slices/messagingSlice';


const BackButton = () => {

  const dispatch = useDispatch();

  const goBack = () => {
    dispatch(goToLobby());
    dispatch(showHUD(false));
  };
  return (
    <Box style={{cursor: 'pointer'}} onClick={goBack} zIndex='100' bg='rgba(0, 0, 0, 0.8)'>
      <Text color='white'><ChevronLeftIcon boxSize='7' /></Text>
    </Box>);
};

export default BackButton;
