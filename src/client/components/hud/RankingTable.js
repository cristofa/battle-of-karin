import React from 'react';
import {Flex, Box, Slide, Center, Tr, Td, Table, Thead, Th, Tbody, Avatar} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';
import PropTypes from 'prop-types';

import {getSorted} from '../../app/slices/playersSlice';

const RankingTable = (props) => {
  const { t } = useTranslation();

  const rankingEntries = useSelector((state) => getSorted(state));
  const socketId = useSelector(state => state.game.socketId);
  const getTable = (entries, offset=0) => {
    const list = entries.map((e, i) =>
      <Tr key={i} bg={e.id == socketId ? 'blue' : ''}>
        <Td color='white'>{i+1+offset}</Td>
        <Td color='white'>{e.rank >= 0 ? <Avatar size='2xs' src={`/assets/images/rank${e.rank}.png`}/> : null} {e.name}</Td>
        <Td color='white' isNumeric>{e.points}</Td>
      </Tr>);
    return  <Box mb='50px' p='25px' pb='5px' bg='rgba(0, 0, 0, 0.8)'>
      <Table size='sm'>
        <Thead>
          <Tr>
            <Th color='white'>#</Th>
            <Th color='white'>{t('pilotName')}</Th>
            <Th color='white' isNumeric>{t('score')}</Th>
          </Tr>
        </Thead>
        <Tbody>{list}</Tbody>
      </Table>
    </Box>;
  };

  const rankLen = rankingEntries.length > 10 ? Math.ceil(rankingEntries.length/2) : 10;
  const table1 = getTable(rankingEntries.slice(0, rankLen));
  let table2 = null;
  if (rankingEntries.length > rankLen) {
    table2 = getTable(rankingEntries.slice(rankLen), rankLen);
  }

  return (
    <Slide in={props.visible} direction='top'>
      <Center>
        <Flex direction='row'>
          {table1}
          {table2}
        </Flex>
      </Center>
    </Slide>);
};

RankingTable.propTypes = {
  visible: PropTypes.any
};
export default RankingTable;
