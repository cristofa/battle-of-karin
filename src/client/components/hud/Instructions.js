import React from 'react';
import {
  Heading, Box, Center, Fade, Image, Flex
} from '@chakra-ui/react';
import {useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';


const Instructions = () => {
  const { t } = useTranslation();

  const instructions = useSelector(state => state.game.instructionsVisible);
  const warmup = useSelector(state => state.game.warmup);

  return (
    <React.Fragment>
      <Fade in={instructions && !warmup} zIndex='1'>
        <Center>
          <Box w='300px'bottom='30px' pos='absolute' p='25px' pb='5px' bg='rgba(0, 0, 0, 0.8)'>
            <Flex direction='row' align='center'>
              <Image h='80px' src='/assets/images/space.png' alt='space' />
              <Image h='80px' src='/assets/images/arrows.png' alt='Arrows' />
            </Flex>
            <Heading align='center' color='white' fontSize='lg'>{t('pressAny')}</Heading>
          </Box>
        </Center>
      </Fade>
    </React.Fragment>);
};

export default Instructions;
