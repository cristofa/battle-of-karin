import React from 'react';
import {Flex} from '@chakra-ui/react';
import {useSelector} from 'react-redux';

import KillMessages from './KillMessages';
import StreakMessage from './StreakMessage';
import DeathScreen from './DeathScreen';
import VictimMessage from './VictimMessage';
import Instructions from './Instructions';
import Timer from './Timer';
import Console from './Console';
import Buttons from './Buttons';
import Ranking from './Ranking';

const HUD = () => {
  const warmup = useSelector(state => state.game.warmup);

  return (

    <Flex direction='column' pos='absolute' top='0' left='0' w='100%' h='100%'>
      <Buttons/>
      <Ranking/>
      {warmup ? null : <DeathScreen />}
      {warmup ? null : <Instructions/>}
      <VictimMessage/>
      <Console/>
      <KillMessages/>
      <StreakMessage/>
      <Timer/>
    </Flex>);
};

export default HUD;
