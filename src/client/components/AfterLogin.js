import React, {useEffect} from 'react';
import {Box, Text, Button} from '@chakra-ui/react';
import {useTranslation} from 'react-i18next';


const AfterLogin = () => {
  const { t } = useTranslation();
  const closePopup = () => {
    window.opener.postMessage('authenticated');
    window.close();
  };
  useEffect(() => {
    closePopup();
  });

  return (
    <Box>
      <Text> {t('clickToClose')} <Button onClick={closePopup}>{t('close')}</Button></Text>
    </Box>
  );
};

export default AfterLogin;
