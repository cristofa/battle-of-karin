import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { Fade } from '@chakra-ui/react';
import loadable from '@loadable/component';

import fetchUserDetails from './lobby/common/fetchPlayerDetails';


const Lobby = loadable(() => import('./lobby/Lobby'));
const HUD = loadable(() => import('./hud/HUD'));

const Overlay = () => {

  const visible = useSelector(state => state.game.uiVisible);
  const showingHUD = useSelector(state => state.game.showingHUD);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!showingHUD) {
      fetchUserDetails(dispatch);
    }
  }, [showingHUD]);

  return <Fade in={visible}>
    { showingHUD ?  <HUD/> : <Lobby/>}
  </Fade>;
};

export default Overlay;
