import React from 'react';
import {Flex} from '@chakra-ui/react';

import Header from './Header';
import Stats from './Stats';
import Footer from './Footer';
import CentralPane from './CentralPane';
import Customize from './Customize';

const Lobby = () => {


  return (
    <Flex direction='column' pos='absolute' top='0' left='0' w='100vw' h='100vh'>
      {/*Header*/}
      <Header/>
      {/*Content*/}
      <Flex direction='row' w='100%' h='100%'>
        {/* content */}
        <CentralPane/>
        {/* Stats */}
        <Stats/>

      </Flex>
      {/*Footer*/}
      <Footer/>
      {/* Customize player*/}
      <Customize/>
    </Flex>);
};

export default Lobby;
