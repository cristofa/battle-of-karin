import React from 'react';
import {Button, Flex, Heading, Spacer, Box, Center} from '@chakra-ui/react';
import {SettingsIcon} from '@chakra-ui/icons';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';

import getRank from './common/getRank';
import {showCustomize} from '../../app/slices/gameSlice';

const Footer = () => {
  const { t } = useTranslation();
  const player = useSelector(state => state.game.player);
  const dispatch = useDispatch();
  const isCustomizing = useSelector(state => state.game.customize);

  if (!player.userName) {
    return null;
  }

  return (
    <Box  w='50%' pos='absolute' left='0px' bottom='0px' style={{display: player.userName ? 'block' : 'none'}}>
      <Flex borderRadius='md' direction='row' bg='white' pt='20px' pb='20px' w='100%'>
        <Center minWidth='120px' w='12%'>
          <Button ml='10px' disabled={isCustomizing} onClick={() => dispatch(showCustomize(true))} leftIcon={<SettingsIcon />} colorScheme='brand' variant='solid'>
            {t('customize')}
          </Button>
        </Center>
        <Flex direction='column'>
          <Flex wrap='wrap' w='100%' direction='row' align='center' pl='3'>
            <Heading p='10px' size='sm'>{t('pilot')}:</Heading><Heading size='md'>{player.userName}</Heading>
            <Spacer/>
            <Heading p='10px' size='sm'>{t('footerRank')}:</Heading><Heading size='md'> {t(`rank${player.rank}`)}</Heading>
            <Spacer/>
            <Heading p='10px' size='sm'>{t('totalScore')}:</Heading><Heading size='md'> {player.totalScore}</Heading>
            <Spacer/>
            <Heading p='10px' size='sm'>{t('wonRounds')}:</Heading><Heading size='md'> {player.wonRounds}</Heading>
          </Flex>
          {player.badges.length > 0 ?
            <Flex wrap='wrap' w='100%' direction='row' pl='3' align='center'>
              <Heading p='10px' size='sm' colorScheme='red'>{t('Badges')}:</Heading>
              {player.badges.map(b => getRank(b.major, b.minor, 2))}
            </Flex>
            : null}
        </Flex>
      </Flex>
    </Box>);
};

export default Footer;
