import React from 'react';
import {Button, Flex, Box} from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import joinGameCallback from './common/joinGameCallback';

const JoinGame = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const player = useSelector(state => state.game.player);
  const gameHostIP = useSelector(state => state.game.gameHostIP);

  return (
    <Flex direction='row' align='center' justify='center' flex='1'>
      <Box p='20px' bg='white' borderRadius='md'>
        <Button colorScheme='brand' onClick={() => {joinGameCallback(dispatch, gameHostIP, player.id);}}>
          {t('joinGame')}
        </Button>
      </Box>
    </Flex>);
};

export default JoinGame;
