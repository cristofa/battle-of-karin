import {getHost} from '../../../utils';
import {changeColors} from '../../../app/slices/messagingSlice';
import {setPlayerDetails} from '../../../app/slices/gameSlice';

const fetchUserDetails = (dispatch) => {
  fetch(`${getHost()}/user/details`, {
    credentials: 'include'
  }).then(res => {
    if (!res.ok) {
      throw new Error('Can\'t fetch user details');
    }
    return res.json();
  }).then((result) => {
    dispatch(setPlayerDetails(result));
    dispatch(changeColors(result));
  }).catch(e => {
    console.log(e.message);
  });
};

export default fetchUserDetails;
