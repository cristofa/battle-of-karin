import {Avatar} from '@chakra-ui/react';
import React from 'react';


const RANK_TO_BG = ['yellow.200', 'gray', 'yellow.500'];

const getRank = (major, minor, margin = 0) => {
  return <Avatar m={margin} size='xs' bg={RANK_TO_BG[minor]} src={`/assets/images/rank${major}.png`}/>;
};

export default getRank;
