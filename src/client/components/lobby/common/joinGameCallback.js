
import {getGameHost} from '../../../utils';
import {playGame} from '../../../app/slices/messagingSlice';
import {setLoading, setNamespace, showError} from '../../../app/slices/gameSlice';
import {ERROR} from '../../../../common/constants';

const playCallback = (dispatch, gameHostIP, playerId) => {
  dispatch(setLoading(true));
  const urlParams = new URLSearchParams(window.location.search);
  const namespace = urlParams.get('uuid');

  fetch(`${getGameHost(gameHostIP)}/game/namespace/${namespace || ''}`).then(res => {
    if (res.status == 503) {
      dispatch(showError(ERROR.serverFull));
      throw new Error('Can\'t connect to game. Server is full.');
    } else if (res.status == 500) {
      dispatch(showError(ERROR.general));
      throw new Error('Can\'t connect to game. General error.');
    }
    return res.json();
  }).then((result) => {
    dispatch(setNamespace({namespace: result.ns}));
    dispatch(playGame({gameHostIP, namespace: result.ns, playerId}));
  }).catch(e => {
    console.log(e.message);
  });
};

export default playCallback;
