import React, {useEffect} from 'react';
import {Button, Stack, Image, Text} from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import commonConfig from '../../../common/commonConfig';
import {getHost} from '../../utils';
import joinGameCallback from './common/joinGameCallback';
import fetchUserDetails from './common/fetchPlayerDetails';

const Login = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const gameHostIP = useSelector(state => state.game.gameHostIP);

  const playCallback = () => {
    joinGameCallback(dispatch, gameHostIP);
  };

  const loginCallback = () => {
    window.open(`${getHost()}/${commonConfig.authPath}`, 'blank');
  };

  useEffect(() => {
    fetchUserDetails(dispatch);
    window.addEventListener('message', (event) => {
      if (event.origin !== `${getHost(true)}` || event.data !== 'authenticated') {
        return;
      }
      fetchUserDetails(dispatch);
    }, false);
  }, []);

  return (
    <React.Fragment>
      <Text align='center' fontSize='md'>{t('loginToCount')}</Text>
      <Stack spacing='25px' justify='center' direction='row' align='center' p='10px' w='100%'>
        <Button colorScheme='brand' onClick={loginCallback}>
          <Image pr='10px' htmlHeight='30px' htmlWidth='30px' src='/assets/images/discord.png' alt='Discord' />
          {t('login')}
        </Button>
        <Button colorScheme='gray' onClick={playCallback}>
          {t('playAsGuest')}
        </Button>
      </Stack>
    </React.Fragment>
  );
};

export default Login;
