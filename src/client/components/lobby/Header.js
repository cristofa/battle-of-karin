import React from 'react';
import {
  Button,
  Flex,
  Spacer,
  ButtonGroup,
  Image,
  Link,
  Heading,
  useDisclosure,
  Modal,
  ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, Text
} from '@chakra-ui/react';
import {InfoIcon} from '@chakra-ui/icons';
import { useTranslation } from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';

import LocaleSelect from './LocaleSelect';
import {getHost} from '../../utils';
import {setPlayerDetails} from '../../app/slices/gameSlice';

const Header = () => {
  const { t } = useTranslation();
  const {userName} = useSelector(state => state.game.player);

  const dispatch = useDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const logout = () => {
    fetch(`${getHost()}/auth/logout`, {
      credentials: 'include'
    }).then(result => {
      if (result.ok) {
        dispatch(setPlayerDetails({}));
      }
    });
  };

  const logoutButton = (
    <Button colorScheme='gray' variant='outline' onClick={logout}>
      {t('logout')}
    </Button>);

  return (
    <div>
      <Flex direction='row' w='100%' bg='white' p='2' pl='3' pr='1' align='center'>
        <Link href='https://battleofkarin.eu' isExternal pr='15px'>
          <Text fontSize='xl' as='u'>Battle of Karin</Text>
        </Link>
        <Link href='https://discord.gg/9J7GsZfmam' isExternal pr='15px'>
          <Image boxSize='35px' src='/assets/images/discord.login.png' alt='Discord' />
        </Link>

        <Link href='https://iogames.space' isExternal pr='15px'>
          <Image boxSize='35px' w={'120px'} src='/assets/images/iogames-logo.png' alt='IoSpace' />
        </Link>
        <Spacer/>
        <ButtonGroup spacing='25px'>
          {userName ? logoutButton : null}

          <Button onClick={onOpen} leftIcon={<InfoIcon />} colorScheme='blue' variant='solid'>
            {t('info')}
          </Button>
          <LocaleSelect/>
        </ButtonGroup>
      </Flex>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{t('info_header')}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Heading size='sm'>{t('info_line1')}</Heading>
            <Text pt='5px'>{t('info_line2')}</Text>
            <Heading size='sm' pt='10px'>{t('info_line3')}</Heading>
            <Text pt='5px'>{t('info_line4')}</Text>
            <Heading size='sm' pt='10px'>{t('info_line5')}</Heading>
            <Text pt='5px'>{t('info_line6')}</Text>
            <Heading size='sm' pt='10px'>{t('info_line7')}</Heading>
            <Text pt='5px'>{t('info_line81')}
              <Link isExternal color='blue.500' pr='2px' pl='2px' href='https://discord.gg/9J7GsZfmam'>
                {t('info_link')}
              </Link>
              {t('info_line82')}
            </Text>
          </ModalBody>
        </ModalContent>
      </Modal>
    </div>);
};

export default Header;
