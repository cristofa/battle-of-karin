import React, {useEffect, useState} from 'react';
import {Button, Box, Slide, VStack, FormControl, FormLabel, Input, FormErrorMessage, CloseButton} from '@chakra-ui/react';
import {CheckIcon} from '@chakra-ui/icons';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {SliderPicker} from 'react-color';

import {setPlayerDetails, showCustomize, setLoading, showError} from '../../app/slices/gameSlice';
import {changeColors} from '../../app/slices/messagingSlice';
import {getHost} from '../../utils';
import {ERROR} from '../../../common/constants';


const Customize = () => {
  const { t } = useTranslation();
  const player = useSelector(state => state.game.player);
  const dispatch = useDispatch();
  const isCustomizing = useSelector(state => state.game.customize);
  const [userName, setUserName] = useState(player.userName);
  const [bodyColor, setBodyColor] = useState(player.bodyColor);
  const [cockpitColor, setCockpitColor] = useState(player.cockpitColor);
  const [flameColor, setFlameColor] = useState(player.flameColor);

  useEffect(() => {
    setUserName(player.userName);
    setBodyColor(player.bodyColor);
    setCockpitColor(player.cockpitColor);
    setFlameColor(player.flameColor);
  }, [player]);

  const updateShip = () => {
    dispatch(changeColors({bodyColor, cockpitColor, flameColor}));
  };

  const changePilotName = (e) => {
    setUserName(e.target.value);
  };
  const changeBodyColor = (color) => {
    setBodyColor(color.hex);
  };
  const changeCockpitColor = (color) => {
    setCockpitColor(color.hex);
  };
  const changeFlameColor = (color) => {
    setFlameColor(color.hex);
  };

  useEffect(() => {
    if (bodyColor && cockpitColor && flameColor) {
      updateShip();
    }
  });

  const close = () => {
    dispatch(showCustomize(false));
  };

  const savePlayer = () => {
    dispatch(setLoading(true));
    fetch(`${getHost()}/user/details`, {
      credentials: 'include',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        userName,
        bodyColor,
        cockpitColor,
        flameColor
      })
    })
      .then(res => {
        if (res.status == 200) {
          console.log('REturn JSON');
          return res.json();
        } else {
          console.log('REturn null');
          return null;
        }
      })
      .then(json => {
        console.log('result ' + JSON.stringify(json));

        if (json) {
          dispatch(setPlayerDetails(json));
          dispatch(setLoading(false));
          close();
        } else {
          dispatch(showError(ERROR.general));
        }
      });
  };

  if (typeof userName == 'undefined') {
    return null;
  }
  const isNameMissing = userName.length == 0;
  return (
    <Slide in={isCustomizing} unmountOnExit='true' direction='left' style={{ width: '380px'}} >
      <Box borderRadius='md' position='absolute' bottom='0px' left='0px' direction='column' h='420px' w='380px' bg='white' pb='20px'>
        <CloseButton onClick={close} float='right'/>
        <VStack w='380px' p='15px'  pt='0px'>
          <FormControl id='pilot-name' isRequired isInvalid={isNameMissing}>
            <FormLabel>{t('pilotName')}</FormLabel>
            <Input placeholder={t('pilotName')} value={userName} onChange={changePilotName} />
            <FormErrorMessage>{t('pilotNameEditError')}</FormErrorMessage>
          </FormControl>
          <FormControl id='body-color'>
            <FormLabel>{t('shipBodyColor')}</FormLabel>
            <SliderPicker widht='20%' color={bodyColor} onChange={changeBodyColor} onChangeComplete={changeBodyColor} />
          </FormControl>
          <FormControl id='cockpit-color'>
            <FormLabel>{t('shipCockpitColor')}</FormLabel>
            <SliderPicker widht='20%' color={cockpitColor} onChange={changeCockpitColor} onChangeComplete={changeCockpitColor} />
          </FormControl>
          <FormControl id='flame-color'>
            <FormLabel>{t('shipFlameColor')}</FormLabel>
            <SliderPicker widht='20%' color={flameColor} onChange={changeFlameColor} onChangeComplete={changeFlameColor} />
          </FormControl>
        </VStack>
        <Button disabled={isNameMissing} float='right' mr='20px' colorScheme='brand'
          onClick={savePlayer}><CheckIcon pr='5px'/>Save</Button>
      </Box>
    </Slide>
  );
};

export default Customize;
