import React, {useState} from 'react';
import {Select} from '@chakra-ui/react';
import {useTranslation} from 'react-i18next';

import {languages} from '../../app/i18n';

const LocaleSelect = () => {

  const [ lang, setLang ] = useState(window.location != window.parent.location ? 'en' : (localStorage.getItem('language') || 'en'));
  // eslint-disable-next-line no-unused-vars
  const { t, i18n } = useTranslation(['translation']);

  const changeLang = (e) => {
    if (window.location == window.parent.location) {
      localStorage.setItem('language', e.target.value);
    }
    setLang(e.target.value);
    i18n.changeLanguage(e.target.value);
  };

  const options = languages.map((l) => {
    return <option key={l.key} value={l.key}>{l.label}</option>;
  });

  return (
    <Select onChange={changeLang} value={lang} variant='flushed'  w='60px'>
      {options}
    </Select>);
};

export default LocaleSelect;
