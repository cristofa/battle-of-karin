import React, {useEffect} from 'react';
import {Box, Center, Fade, Flex, Heading} from '@chakra-ui/react';
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from 'react-i18next';

import Login from './Login';
import JoinGame from './JoinGame';
import {changeColors} from '../../app/slices/messagingSlice';


const CentralPane = () => {

  const player = useSelector(state => state.game.player);
  const { t } = useTranslation();
  const showPane = useSelector(state => state.game.customize);
  const dispatch = useDispatch();
  const visible = useSelector(state => state.game.uiVisible);

  useEffect(() => {
    if (player.userName) {
      dispatch(changeColors(player));
    }
  }, [player, visible]);
  const content = player.userName ? <JoinGame/> : <Login/>;

  return (
    <Flex direction='row' align='center' justify='center' flex='1' h='100%' w='50%'>
      <Fade in={!showPane}>
        <Box bg='white' borderRadius='md' p='20px'>
          <Center><Heading>{t('gameName')}</Heading></Center>
          {content}
        </Box>
      </Fade>
    </Flex>);
};

export default CentralPane;
