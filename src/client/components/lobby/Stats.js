import React, {useEffect, useState} from 'react';
import {
  Box,
  Flex,
  Spinner,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  TableCaption,
  Heading,
  Center
} from '@chakra-ui/react';
import {useTranslation} from 'react-i18next';
import {useSelector} from 'react-redux';

import {getHost} from '../../utils';
import getRank from './common/getRank';

const RANK_TO_IMG = {
  rank_last_day: 0,
  rank_last_week: 1,
  rank_last_month: 2,
  rank_all_time: 3
};

const getTable = (entries, rankName, t, userId) => {
  const list = entries[rankName].map((e, i) =>
    <Tr id={'tableRow'+i} key={i} border={e.id == userId ? '2px solid' : 'none'} borderColor={e.id == userId ? 'brand.200' : ''}>
      <Td color='black'>{e.position ? '...'+ e.position : getRank(RANK_TO_IMG[rankName], i)}</Td>
      <Td color='black'>{e.userName}</Td>
      <Td color='black' isNumeric>{e.totalScore}</Td>
    </Tr>);

  return  <Box pt='10px' bg='white' m='1px'>
    <Table variant='striped' size='sm' mt='-20px'>
      <TableCaption placement="top">
        <Heading color={rankName == 'rank_all_time' ? 'brand.700' : ''}
          size={rankName == 'rank_all_time' ? 'md' : 'sm'}>{t(rankName)}</Heading>
      </TableCaption>
      <Thead>
        <Tr>
          <Th color='black'>#</Th>
          <Th color='black'>{t('pilot')}</Th>
          <Th color='black' isNumeric>{t('score')}</Th>
        </Tr>
      </Thead>
      <Tbody>{list}</Tbody>
    </Table>
  </Box>;
};

const Stats = () => {
  const { t } = useTranslation();
  const [stats, setStats] = useState(null);
  const player = useSelector(state => state.game.player);

  const fetchStats = () => {
    fetch(`${getHost()}/result/${player.userName ? player.id : ''}`, {
      credentials: 'include'
    }).then(res => {
      if (!res.ok) {
        throw new Error('Can\'t fetch stats');
      }
      return res.json();
    }).then((result) => {
      setStats(result);
    }).catch(e => {
      console.log(e.message);
    });
  };

  useEffect(() => {
    fetchStats();
  }, [player]);

  const playerId = player.id;

  return (
    <Box bg='white' minWidth='300px' width='30%' maxHeight='70vh' minHeight='100%' maxWidth='50%' overflow='auto'
      borderRadius='md'>
      { stats ?
        <Flex direction='column' h='100%' justify='space-between'>
          {getTable(stats, 'rank_all_time', t, playerId)}
          {getTable(stats, 'rank_last_month', t, playerId)}
          {getTable(stats, 'rank_last_week', t, playerId)}
          {getTable(stats, 'rank_last_day', t, playerId)}
        </Flex>
        : <Center><Spinner mt='50%' color='brand.500' size='lg' thickness='3px'/></Center>}
    </Box>);
};

export default Stats;
