import React from 'react';
import {useSelector} from 'react-redux';
import { Modal, ModalOverlay, ModalContent, ModalBody, Text,  Center, Spinner } from '@chakra-ui/react';
import {useTranslation} from 'react-i18next';


const ModalDialog = () => {

  const error = useSelector(state => state.game.error);
  const isLoading = useSelector(state => state.game.isLoading);

  const { t } = useTranslation();

  const isOpen = error || isLoading;
  let content = <Center position='fixed' top='0' width='100%' height='100%'>
    <Spinner color='brand.500' size='xl' thickness='4px'/>
  </Center>;

  if (error) {
    content = <ModalContent>
      <ModalBody>
        <Text fontWeight="bold" mb="1rem">
          {t(`errorHeader${error}`)}
        </Text>
        {t(`errorContent${error}`)}
      </ModalBody>
    </ModalContent>;
  }
  return (<Modal isOpen={isOpen} isCentered='true'>
    <ModalOverlay/>
    {content}
  </Modal>);
};

export default ModalDialog;
