import React, {useEffect} from 'react';
import Phaser from 'phaser';
import {useDispatch, useSelector} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Helmet} from 'react-helmet';

import {
  setUIVisible,
  showHUD,
  setSocketId,
  showKill,
  showDeathScreen,
  markMessage,
  showInstructions,
  setTime,
  setLoading,
  showWarmupMsg,
  showConsoleMsg,
  showVictimMsg,
  showError
} from '../app/slices/gameSlice';
import {updatePlayers, updatePlayer, removePlayer} from '../app/slices/playersSlice';
import gameConfig from '../game/gameConfig';
import gameMiddlewares from '../app/gameMiddlewares';
import {getWsPort} from '../utils';


const Game = () => {
  const dispatch = useDispatch();
  const gameHostIP = useSelector(state => state.game.gameHostIP);

  useEffect(() => {
    const game = new Phaser.Game(gameConfig);
    game.overlay = bindActionCreators(
      {setUIVisible,
        showHUD,
        updatePlayers,
        setSocketId,
        updatePlayer,
        showKill,
        markMessage,
        showVictimMsg,
        removePlayer,
        showInstructions,
        setTime,
        setLoading,
        showWarmupMsg,
        showConsoleMsg,
        showError,
        showDeathScreen},
      dispatch);
    game.middlewares = gameMiddlewares;
  }, []);

  return (
    <React.Fragment>
      <Helmet>
        <script src={`${process.env.NODE_ENV === 'development' ? 'http:' : 'https:'}//${gameHostIP}${getWsPort()}/socket.io/socket.io.js`}></script>
      </Helmet>
      <div id='game' style={{width: '100vw', height: '100vh'}}></div>
    </React.Fragment>);
};

export default Game;
