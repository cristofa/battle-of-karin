export default {
  host: process.env.HOST || '127.0.0.1',
  port: process.env.PORT || 3000,
  gameHosts:  [
    {name: 'server_europe', host:  process.env.GAME_HOST_EU || '127.0.0.1'}
  ],
  wsPort: process.env.WS_PORT || 3200,
  gameApiPort: process.env.API_PORT || 3200,
  authPath: 'auth/login',
  loginRedirect: '/afterLogin',
  game: {
    size: {width: 4000, height: 4000},
    streaks: [3, 5, 7, 10],
    times: {
      respawnTime: 5,
      round: 180,
      warmup: 10,
    }
  }
};

