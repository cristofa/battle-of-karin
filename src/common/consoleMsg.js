const CONSOLE = {
  FIRST_BLOOD: 'firstBlood',
  GO_BERSERK: 'goBerserk',
  BERSERK_OFF: 'berserkOff',
  JOINED: 'joined',
  LEFT: 'left',
  CHANGING_GAME: 'changingGame',
  RESULTS_SENT: 'resultsSent',
  MAINTENANCE: 'maintenance'
};

export default CONSOLE;
