const CONST = {

};

const ERROR = {
  general: 'GENERAL',
  kickedOut: 'KICKED_OUT',
  serverFull: 'SERVER_FULL'
};
export { ERROR };

export default CONST;
