const hexCode = '0123456789ABCDEF';
const randomColor = () => {
  let result = '#';
  for (let i = 0; i < 6; i++) {
    result += hexCode[Math.floor(Math.random() * 16)];
  }

  return result;
};
export {randomColor};
