const NET_MSGS = {
  DISCONNECT: 'disconnected',
  CONNECT: 'connect',
  LOGIN: 'login',
  TIME: 'time',
  WINNERS: 'winners',
  CONSOLE: 'console',
  ERROR: 'error',
  KICKED_OUT: 'kickedOut',
  PLAYER: {
    UPDATE: 'playerUpdates',
    RANKING: 'playersRanking',
    NEW: 'newPlayer',
    KILL: 'playerKill',
    ACCELERATE: 'playerAccelerate',
    RAILGUN_SHOT: 'railgunShot',
    INIT: 'initPlayers',
    INPUT: 'playerInput',
    MSG_FROM_VICTIM: 'msgFromVictim',
    MSG_TO_KILLER: 'msgToKiller',
    WARMUP_MSG: 'warmupMsg',
    CHANGE_GAME: 'changeGame'
  },
};

export default NET_MSGS;
