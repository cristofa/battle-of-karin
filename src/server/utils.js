
const checkAuth = (req, res, next) => {
  if (req.isAuthenticated()) return next();
  res.status(401);
  res.json({e: 'Something went wrong. Try to log in again.'});
};

export { checkAuth };
