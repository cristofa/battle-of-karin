import app, {dbConnection, Badge} from './server';
import commonConfig from '../common/commonConfig';
import {initViews, initBadges} from './db/db';

const startServer = (app, dbConnection, Badge, port) => {
  return new Promise((resolve, reject) => {

    dbConnection.authenticate().then(async () => {
      if (process.env.NODE_ENV === 'development') {
        await dbConnection.sync({force: false});
      }

      await initViews(dbConnection);
      await initBadges(dbConnection, Badge);

    }).catch((err) => {
      console.error('Error in game start script.', err);
      process.exit(1);
    });

    const httpServer = app.listen(port);
    httpServer.once('error', (err) => {
      if (err.code === 'EADDRINUSE') {
        reject(err);
      }
    });

    httpServer.once('listening', () => resolve(httpServer));

  }).then((httpServer) => {
    const {port} = httpServer.address();
    console.info(`==> Listening on ${port}. Open up http://${commonConfig.host}:${port}/ in your browser.`);

    // Hot Module Replacement API
    if (module.hot) {
      let currentApp = app;
      module.hot.accept('./server', () => {
        httpServer.removeListener('request', currentApp);
        import('./server')
          .then(({default: nextApp}) => {
            currentApp = nextApp;
            httpServer.on('request', currentApp);
            console.log('HttpServer reloaded!');
          })
          .catch((err) => console.error(err));
      });

      // For reload main module (self). It will be restart http-server.
      module.hot.accept((err) => console.error(err));
      module.hot.dispose(() => {
        console.log('Disposing entry module...');
        httpServer.close();
      });
    }
  });
};
console.log('Starting http server...');
startServer(app, dbConnection, Badge, commonConfig.port).catch((err) => {
  console.error('Error in server start script.', err);
});
