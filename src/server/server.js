import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import session from 'express-session';
import { BasicStrategy } from 'passport-http';
import pgStore from 'connect-pg-simple';
import schedule from 'node-schedule';

import path from 'path';
import setupRouter from './controllers';
import config from './config';
import {dbConnect, getDbConnString, dbInit, refreshViews } from './db/db';
import commonConfig from '../common/commonConfig';


const app = express();

const sess = {
  secret: config.sessionSecret,
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24
  }
};

if (app.get('env') === 'production') {
  sess.store = new (pgStore(session))({conString: getDbConnString(config) });
  sess.cookie.secure = true;
  app.set('trust proxy', 1);

}

app.use(session(sess));

passport.use(new BasicStrategy(
  function(user, password, done) {
    if (user == config.basicAuthUser && password == config.basicAuthPass) {
      return done(null, true);
    }
    return done(null, false);
  }
));
app.use(passport.initialize());
app.use(passport.session());

app.use(express.static('public'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
const corsOptions = {
};


if (process.env.NODE_ENV === 'development') {
  app.use(express.static('build-game'));
  app.use(express.static('build-client'));

  corsOptions.origin = `http://${commonConfig.host}:3001`;
  corsOptions.credentials = true;
}

app.use(cors(corsOptions));
const dbConnection = dbConnect(config);
const { User, Score, Game, Badge } = dbInit(dbConnection);

app.use('/', setupRouter(User, Game, Score, Badge, dbConnection));


app.get('*', (req,res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

const rule = new schedule.RecurrenceRule();
rule.minute = 0;

schedule.scheduleJob(rule, async () => {
  const now = new Date();
  console.log(`Refreshing db views at: ${now.toLocaleString()}`);
  await refreshViews(dbConnection);
});

export {dbConnection, User, Game, Score, Badge};

export default app;
