import request from 'supertest';
import express from 'express';

import app, {User, dbConnection} from '../server';

let parentApp;

const profileId1 = '666';
const userName = 'test1';

describe('Test user controller', () => {

  beforeAll(async () => {
    await dbConnection.sync({force: true});

    const user = await User.create({userName, profileId: profileId1});

    parentApp = express();
    parentApp.use((req, res, next) => {
      req.isAuthenticated = () => true;
      req.session = {passport: {user}};
      next();
    });
    parentApp.use(app);

  });

  afterAll(async() => {
    await dbConnection.close();
  });

  test('Get user default values', async () => {
    request(parentApp)
      .get('/user/details')
      .then(response => {
        expect(response.statusCode).toBe(200);
        const user = response.body;
        expect(user.userName).toBe(userName);
        expect(user.bodyColor).toBe('#ffffff');
        expect(user.flameColor).toBe('#ffffff');
        expect(user.cockpitColor).toBe('#000000');
      });
  });

  test('Update user data', async () => {
    const otherName = 'pilot2';
    const otherBody = '#aaaaaa';
    const otherCockpit = '#bbbbbb';
    const otherFlame = '#cccccc';

    const response = await request(parentApp)
      .post('/user/details')
      .send({userName: otherName,
        bodyColor: otherBody,
        cockpitColor: otherCockpit,
        flameColor: otherFlame});

    expect(response.statusCode).toBe(200);

    request(parentApp)
      .get('/user/details')
      .then(response => {
        expect(response.statusCode).toBe(200);
        const user = response.body;
        expect(user.userName).toBe(otherName);
        expect(user.bodyColor).toBe(otherBody);
        expect(user.flameColor).toBe(otherFlame);
        expect(user.cockpitColor).toBe(otherCockpit);
      });
  });

  test('User data validation', async () => {
    const otherName = 'testPilot';
    const goodColor = '#aaaaaa';
    const badColor = '#dd';
    let response = await request(parentApp)
      .post('/user/details')
      .send({
        userName: '',
        bodyColor: goodColor,
        cockpitColor: goodColor,
        flameColor: badColor
      });

    expect(response.statusCode).toBe(400);

    response = await request(parentApp)
      .post('/user/details')
      .send({
        userName: otherName,
        bodyColor: badColor,
        cockpitColor: goodColor,
        flameColor: goodColor
      });

    expect(response.statusCode).toBe(400);

    response = await request(parentApp)
      .post('/user/details')
      .send({
        userName: otherName,
        bodyColor: goodColor,
        cockpitColor: badColor,
        flameColor: goodColor
      });

    expect(response.statusCode).toBe(400);

    response = await request(parentApp)
      .post('/user/details')
      .send({
        userName: otherName,
        bodyColor: goodColor,
        cockpitColor: goodColor,
        flameColor: badColor
      });

    expect(response.statusCode).toBe(400);
  });

});
