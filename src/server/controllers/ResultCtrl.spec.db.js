import request from 'supertest';

import app, {User, Game, Score, Badge, dbConnection} from '../server';
import {initViews, initBadges, refreshViews, VIEWS} from '../db/db';
import config from '../config';

let userIds;

describe('Test result controller', () => {

  beforeEach(async () => {
    await dbConnection.sync({force: true});
    userIds = [];
    for (let i = 0; i < 15; i++) {
      const user = await User.create({userName: `user${i}`, profileId: i, totalScore: i*20});
      userIds.push(user.id);
    }
    await initViews(dbConnection);
    await initBadges(dbConnection, Badge);
    for (let i = 0; i < 15; i++) {
      const date = new Date();
      date.setDate(date.getDate() - i);
      const game = await Game.create({ended: date});
      for (const userId of userIds) {
        await Score.create({score: userId, GameId: game.id, UserId: userId});
      }
    }
  });

  afterAll(async (done) => {
    await dbConnection.close();
    done();
  });

  test('Save results', async (done) => {

    await refreshViews(dbConnection);
    const payload = {results: [], maxScore: 1};
    userIds.forEach(id => payload.results.push({id, points: 1}));

    const response = await request(app)
      .post('/result/')
      .auth(config.basicAuthUser, config.basicAuthPass)
      .send(payload);

    expect(response.statusCode).toBe(200);
    const scores = await Score.findAll();

    scores.forEach(s => {
      expect(s.GameId).not.toBeNull();
      expect(userIds).toContain(s.UserId);
    });

    const user15 = await User.findByPk(14);
    expect(user15.totalScore).toBe(271);

    done();

  });

  test('Get stats for user without stats', async (done) => {

    let response = await request(app)
      .get('/result/refresh')
      .auth(config.basicAuthUser, config.basicAuthPass);
    expect(response.statusCode).toBe(200);

    const user = await User.create({userName: 'userNostat', profileId: 666, totalScore: 0});

    response = await request(app)
      .get(`/result/${user.id}`)
      .auth(config.basicAuthUser, config.basicAuthPass);
    expect(response.statusCode).toBe(200);
    done();
  });

  test('Get stats without user', async (done) => {

    let response = await request(app)
      .get('/result/refresh')
      .auth(config.basicAuthUser, config.basicAuthPass);
    expect(response.statusCode).toBe(200);

    response = await request(app)
      .get('/result/');

    expect(response.statusCode).toBe(200);
    let stats = response.body;
    expect(Object.keys(stats).length).toBe(4);
    Object.values(VIEWS).forEach(view => {
      expect(stats[view].findIndex(s => s.id == userIds[14])).toBe(0);
      expect(stats[view].findIndex(s => s.id == userIds[13])).toBe(1);
    });

    done();
  });

  test('Get stats for specific user', async (done) => {

    let response = await request(app)
      .get('/result/refresh')
      .auth(config.basicAuthUser, config.basicAuthPass);
    expect(response.statusCode).toBe(200);

    response = await request(app)
      .get(`/result/${userIds[0]}`);
    expect(response.statusCode).toBe(200);
    let stats = response.body;
    expect(Object.keys(stats).length).toBe(4);
    Object.values(VIEWS).forEach(view => {
      expect(stats[view].findIndex(s => s.id == userIds[13])).toBe(1);
      expect(stats[view].findIndex(s => s.id == userIds[14])).toBe(0);
      expect(stats[view].findIndex(s => s.id == userIds[0])).toBe(3);
      expect(stats[view].length).toBe(4);
    });

    response = await request(app)
      .get(`/result/${userIds[5]}`);
    expect(response.statusCode).toBe(200);
    stats = response.body;
    expect(Object.keys(stats).length).toBe(4);
    Object.values(VIEWS).forEach(view => {
      expect(stats[view].length).toBe(4);
      expect(stats[view].findIndex(s => s.id == userIds[13])).toBe(1);
      expect(stats[view].findIndex(s => s.id == userIds[14])).toBe(0);
      expect(stats[view].findIndex(s => s.id == userIds[5])).toBe(3);
    });

    response = await request(app)
      .get(`/result/${userIds[14]}`);
    expect(response.statusCode).toBe(200);
    stats = response.body;
    expect(Object.keys(stats).length).toBe(4);
    Object.values(VIEWS).forEach(view => {
      expect(stats[view].length).toBe(3);
      expect(stats[view].findIndex(s => s.id == userIds[14])).toBe(0);
      expect(stats[view].findIndex(s => s.id == userIds[13])).toBe(1);
      expect(stats[view].findIndex(s => s.id == userIds[12])).toBe(2);
    });

    done();
  });

  test('Test assign rank', async (done) => {

    await refreshViews(dbConnection);
    const payload = {results: [], maxScore: 70};
    userIds.forEach(id => payload.results.push({id, points: (id == 1 ? 70 : 1)}));

    let user1 = await User.findByPk(1, {include: Badge});
    expect(user1.currentRank).toBe(-1);

    for (let major = 0; major < 4; major++) {
      let response = await request(app)
        .post('/result/')
        .auth(config.basicAuthUser, config.basicAuthPass)
        .send(payload);
      expect(response.statusCode).toBe(200);

      response = await request(app)
        .get('/result/refresh')
        .auth(config.basicAuthUser, config.basicAuthPass);
      expect(response.statusCode).toBe(200);

      user1 = await User.findByPk(1, {include: Badge});
      expect(user1.currentRank).toBe(major);
      expect(user1.Badges.length).toBe(major+1);
    }

    expect(user1.Badges.length).toBe(4);
    done();

  });

});
