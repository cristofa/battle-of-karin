import express from 'express';
import passport from 'passport';

import {refreshViews, VIEWS} from '../db/db';

const RANK_SIZE = 3;
const WINNER_BONUS = 10;

const RANK_TO_MAJOR = {
  LAST_DAY: 0,
  LAST_WEEK: 1,
  LAST_MONTH: 2,
  ALL_TIME: 3
};
class ResultCtrl {
  _controller = express.Router();

  constructor(Game, Score, User, Badge, sequelize) {

    /**
     * Save results in DB
     */
    this._controller.post('/', passport.authenticate('basic', { session: false }), async (req, res) => {

      if (req.body.results.length > 0) {
        const game = await Game.create({ended: new Date()});

        for (const s of req.body.results) {
          const user = await User.findByPk(s.id);

          if (s.points == req.body.maxScore && s.points > 0 ) {
            s.points += WINNER_BONUS;
            user.wonRounds += 1;
          }
          await Score.create({score: s.points, GameId: game.id, UserId: s.id});
          user.totalScore += s.points;
          await user.save();
        }
      }

      res.status(200);
      res.send({});
    });

    /**
     * Get statistics for all users. Used by unauthenticated users
     */
    this._controller.get('/', async (req, res) => {
      const scores = await this.getStatistics(sequelize);
      res.status(200);
      res.send(scores);
    });

    /**
     * Recalculate the ranking views in the DB.
     */
    this._controller.get('/refresh', passport.authenticate('basic', { session: false }), async (req, res) => {
      await refreshViews(sequelize);


      for (let view of Object.keys(VIEWS)) {
        const topPlayers = await sequelize.query(`SELECT * FROM ${VIEWS[view]} limit 3`, {
          raw: true
        });
        let minor = 0;
        let major = RANK_TO_MAJOR[view];

        for (let player of topPlayers[0]) {
          const user = await User.findByPk(player.id, {include: Badge});
          const badge = await Badge.findOne({where: {major, minor}});

          user.addBadge(badge);
          minor++;
          if (major > user.currentRank) {
            user.currentRank = major;
            user.save();
          }
        }
      }


      res.status(200);
      res.send({});
    });

    /**
     * Get statistics for all users. Used by authenticated users. User with id pass as argument will be included
     * in results.
     */
    this._controller.get('/:userId', async (req, res) => {

      const user = await User.findByPk(req.params.userId);
      if (!user) {
        res.status(404);
        res.send({});
      }
      const scores = await this.getStatistics(sequelize, req.params.userId);

      res.status(200);
      res.send(scores);
    });
  }

  async getStatistics(sequelize, userId) {
    const scores = {};
    for (let view of Object.values(VIEWS)) {

      let userRank;
      if (userId) {
        const userRankCount = await sequelize.query(`select count(*) from ${view} where "totalScore" > (select "totalScore" from ${view} where id = ${userId})`, {
          raw: true
        });
        userRank = Number.parseInt(userRankCount[0][0].count);
      }

      const score = await sequelize.query(`SELECT * FROM ${view} limit ${RANK_SIZE}`, {
        raw: true
      });

      scores[view] = score[0];

      if (userRank >= RANK_SIZE) {
        const rawScore = await sequelize.query(`select * from ${view} where id = ${userId};`, {
          raw: true
        });
        const userScore = rawScore[0][0];
        userScore.position = userRank + 1;
        scores[view].push(userScore);
      }

    }
    return scores;
  }

  get controller() {
    return this._controller;
  }
}

export default ResultCtrl;
