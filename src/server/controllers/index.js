import express from 'express';

import AuthCtrl from './AuthCtrl';
import UserCtrl from './UserCtrl';
import ResultCtrl from './ResultCtrl';

const setupRouter = (User, Game, Score, Badge, sequelize) => {

  const router = express.Router();
  const authCtrl = new AuthCtrl(User);
  const userCtrl = new UserCtrl(User, Badge);
  const resultCtrl = new ResultCtrl(Game, Score, User, Badge, sequelize);
  router.use('/auth', authCtrl.controller);
  router.use('/user', userCtrl.controller);
  router.use('/result', resultCtrl.controller);

  return router;
};

export default setupRouter;
