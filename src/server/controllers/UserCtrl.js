import express from 'express';
import passport from 'passport';

import {checkAuth} from '../utils';

class UserCtrl {
  _controller = express.Router();

  _isHex(string) {
    return /^#[0-9A-F]{6}$/i.test(string);
  }

  _getUserData(user, withBadges = false) {
    const userData = {id: user.id,
      userName: user.userName,
      bodyColor: user.bodyColor,
      cockpitColor: user.cockpitColor,
      flameColor: user.flameColor,
      rank: user.currentRank,
      totalScore: user.totalScore,
      wonRounds: user.wonRounds,
    };

    if (withBadges) {
      userData.badges = user.Badges.map(b => { return {major: b.major, minor: b.minor};});
    }
    return userData;
  }

  constructor(User, Badge) {

    this._controller.get('/details', checkAuth, async (req, res) => {
      const user = await User.findByPk(req.session.passport.user.id, {include: Badge});
      res.json(this._getUserData(user, true));
    });

    this._controller.get('/:playerId', passport.authenticate('basic', { session: false }), async (req, res) => {
      const user = await User.findByPk(req.params.playerId);
      res.json(this._getUserData(user));
    });

    this._controller.post('/details', checkAuth, async (req, res) => {
      const user = await User.findByPk(req.session.passport.user.id, {include: Badge});

      if (!req.body.userName || !this._isHex(req.body.bodyColor) ||
        !this._isHex(req.body.cockpitColor) || !this._isHex(req.body.flameColor)) {
        res.status(400);
        res.send({error: 'Invalid player details'});
        return;
      }

      Object.assign(user, req.body);
      await user.save();

      res.status(200);
      res.json(this._getUserData(user, true));
    });
  }

  get controller() {
    return this._controller;
  }
}

export default UserCtrl;
