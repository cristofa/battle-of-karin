import express from 'express';
import passport from 'passport';
import Strategy from 'passport-discord';

import config from '../config';
import {checkAuth} from '../utils';
import commonConfig from '../../common/commonConfig';
import {randomColor} from '../../common/commonUtils';

class AuthCtrl {
  _controller = express.Router();

  constructor(User) {
    passport.serializeUser((user, done) => {
      done(null, user);
    });
    passport.deserializeUser((obj, done) => {
      done(null, obj);
    });

    const tokenToProfile = async (accessToken, refreshToken, profile, done) => {
      const [user, created] = await User.findOrCreate({where:
          {profileId: profile.id}});
      if (created) {
        user.userName = profile.username;
        user.bodyColor = randomColor();
        user.cockpitColor = randomColor();
        user.flameColor = randomColor();
        await user.save();
      }
      done(null, user);
    };

    let callbackPort = process.env.NODE_ENV === 'development' ? `:${commonConfig.port}` : '';
    let clientPort = process.env.NODE_ENV === 'development' ? `:${3001}` : '';

    const strategy = new Strategy({
      clientID: config.discordClientId,
      clientSecret: config.discordSecret,
      callbackURL: `http://${commonConfig.host}${callbackPort}/auth/login`,
      scope: ['identify'],
      prompt: 'consent'
    }, tokenToProfile);

    passport.use(strategy);

    this._controller.get('/login', passport.authenticate('discord', {
      session: true,
      successReturnToOrRedirect: `http://${commonConfig.host}${clientPort}/afterlogin`
    }));

    this._controller.get('/logout',checkAuth, (req, res) => {
      req.logout();
      res.status(200);
      res.send('Logged out');
    });

  }


  get controller() {
    return this._controller;
  }
}

export default AuthCtrl;
