import {Sequelize} from 'sequelize';

import {dbInit} from './db';

let sequelize;
let models = {};

const profileId1 = '666';
const profileId2 = '777';

describe('Test models', () => {

  beforeAll(async () => {
    sequelize = new Sequelize('sqlite::memory', {logging: console.log});
    models = dbInit(sequelize);
    await sequelize.sync({ force: true });
    await models.User.create({userName: 'test1', profileId: profileId1});
    await models.User.create({userName: 'test2', profileId: profileId2});

  });

  test('Find all and find by Id', async () => {
    const users = await models.User.findAndCountAll();
    expect(users.count).toBe(2);

    const user1 = await models.User.findOne({ where: { profileId: profileId1}});
    const user2 = await models.User.findOne({ where: { profileId: profileId2}});

    expect(user1.userName).toBe('test1');
    expect(user2.userName).toBe('test2');
  });

  test('Create game with scores', async () => {
    const testScore = 10;
    const user1 = await models.User.findOne({ where: { profileId: profileId1}});
    const game = await models.Game.create();
    await models.Score.create({score: testScore, UserId: user1.id, GameId: game.id});

    const nestedGame = await models.Game.findByPk(game.id, {include: { all: true, nested: true }});

    expect(nestedGame.Scores[0].score).toBe(testScore);
    expect(nestedGame.Scores[0].UserId).toBe(user1.id);
    expect(nestedGame.ended).not.toBeNull();
  });
});
