const userModel = (sequelize, types) => {
  return sequelize.define('User', {
    profileId: {
      type: types.STRING,
      allowNull: false
    },
    userName: {
      type: types.STRING
    },
    bodyColor: {
      type: types.STRING,
      allowNull: false,
      defaultValue: '#ffffff'
    },
    cockpitColor: {
      type: types.STRING,
      allowNull: false,
      defaultValue: '#000000'
    },
    flameColor: {
      type: types.STRING,
      allowNull: false,
      defaultValue: '#ffffff'
    },
    totalScore: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    wonRounds: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    currentRank: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: -1
    }
  }, {
    tableName: 'users',
    indexes: [
      {
        unique: true,
        fields: ['profileId']
      }
    ]
  });
};

export default userModel;
