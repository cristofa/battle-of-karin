const gameMode = (sequelize, types) => {
  return sequelize.define('Game', {
    ended: {
      type: types.DATE,
      allowNull: false,
      defaultValue: types.NOW
    }
  }, {
    tableName: 'games'
  });
};

export default gameMode;
