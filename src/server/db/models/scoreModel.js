const scoreModel = (sequelize, types) => {
  return sequelize.define('Score', {
    score: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    tableName: 'scores',
  });
};

export default scoreModel;
