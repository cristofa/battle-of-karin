const badgeModel = (sequelize, types) => {
  return sequelize.define('Badge', {
    major: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    minor: {
      type: types.INTEGER,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    tableName: 'badges'
  });
};

export default badgeModel;
