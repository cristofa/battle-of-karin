import {Sequelize, DataTypes} from 'sequelize';

import userModel from './models/userModel';
import gameModel from './models/gameModel';
import scoreModel from './models/scoreModel';
import badgeModel from './models/badgeModel';

const VIEWS = {
  LAST_DAY: 'rank_last_day',
  LAST_WEEK: 'rank_last_week',
  LAST_MONTH: 'rank_last_month',
  ALL_TIME: 'rank_all_time'
};

const getDbConnString = (config) => {
  if (process.env.NODE_ENV === 'test') {
    return 'sqlite::memory';
  } else if (process.env.NODE_ENV === 'testdb') {
    return `postgres://${config.testDbUser}:${config.testDbPass}@localhost:${config.dbPort}/${config.testDbName}`;
  }
  return `postgres://${config.dbUser}:${config.dbPass}@${config.dbHost}:${config.dbPort}/${config.dbName}`;
};

const dbConnect = (config) => {
  return new Sequelize(getDbConnString(config), {logging: false});
};

const initViews = async (sequelize) => {
  await sequelize.query(`create materialized view if not exists ${VIEWS.LAST_DAY} as
  select sum(s.score) as "totalScore", u."userName", u.id from scores as s
  inner join games as g on s."GameId" = g.id
  inner join users as u on s."UserId" = u.id where g.ended > now() - INTERVAL '1 DAY'
  group by u."id" order by "totalScore" desc`);

  await sequelize.query(`create materialized view if not exists ${VIEWS.LAST_WEEK} as
  select sum(s.score) as "totalScore", u."userName", u.id from scores as s
  inner join games as g on s."GameId" = g.id
  inner join users as u on s."UserId" = u.id where g.ended > now() - INTERVAL '7 DAY'
  group by u."id" order by "totalScore" desc`);

  await sequelize.query(`create materialized view if not exists ${VIEWS.LAST_MONTH} as
  select sum(s.score) as "totalScore", u."userName", u.id from scores as s
  inner join games as g on s."GameId" = g.id
  inner join users as u on s."UserId" = u.id where g.ended > now() - INTERVAL '30 DAY'
  group by u."id" order by "totalScore" desc`);

  await sequelize.query(`create materialized view if not exists ${VIEWS.ALL_TIME} as
  select "totalScore", "userName", id from users order by "totalScore" desc`);
};

const initBadges = async (sequelize, Badge) => {
  const badges = await Badge.findAll();
  if (badges.length > 0) {
    return;
  }

  for (let major = 0; major < 4; major++) {
    for (let minor = 0; minor < 3; minor++) {
      await Badge.create({major, minor});
    }
  }
};
const refreshViews = async (sequelize) => {
  await sequelize.query(`refresh materialized view ${VIEWS.LAST_DAY}`);
  await sequelize.query(`refresh materialized view ${VIEWS.LAST_WEEK}`);
  await sequelize.query(`refresh materialized view ${VIEWS.LAST_MONTH}`);
  await sequelize.query(`refresh materialized view ${VIEWS.ALL_TIME}`);
};

const dbInit = (sequelize) => {
  const User = userModel(sequelize, DataTypes);
  const Game = gameModel(sequelize, DataTypes);
  const Score = scoreModel(sequelize, DataTypes);
  const Badge = badgeModel(sequelize, DataTypes);

  Game.hasMany(Score);
  Score.belongsTo(Game);

  User.hasMany(Score);
  Score.belongsTo(User);

  User.belongsToMany(Badge, {through: 'UserBadge'});

  return {User, Score, Game, Badge};
};

export {dbConnect, dbInit, getDbConnString, initViews, initBadges, refreshViews, VIEWS};

