export default {
  sessionSecret: process.env.SESSION_SECRET || 'sessionsecret',
  dbName: process.env.DB_NAME || 'karin',
  dbUser: process.env.DB_USER || 'karin',
  dbPass: process.env.DB_PASS || 'karin',
  dbHost: process.env.DB_HOST || 'localhost',
  dbPort: process.env.DB_PORT || '5432',
  discordClientId: process.env.DISCORD_CLIENT_ID || 'mockDiscordId',
  discordSecret: process.env.DISCORD_SECRET || 'mockDiscordSecret',
  testDbName: 'testdb',
  testDbUser: 'testuser',
  testDbPass: 'password',
  basicAuthUser: process.env.BASIC_AUTH_USER || 'karin',
  basicAuthPass: process.env.BASIC_AUTH_PASS || 'karin'
};
