const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const LoadablePlugin = require('@loadable/webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const path = require('path');

const client = {
  name: 'client',
  entry: ['./src/client/client.js'],
  target: 'web',
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: process.env.NODE_ENV === 'development',
              publicPath: 'css',
            },
          },
          'css-loader',
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'css/fonts/',
              publicPath: 'fonts',
            },
          },
        ],
      },
      {
        test: /\.(jpeg|png)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/client/index.html',
      filename: 'index.html',
      inject: 'body'
    }),
    new CopyPlugin({
      patterns: [
        { from: 'src/assets', to: './assets' }
      ],
    }),
    new webpack.ProgressPlugin(),
    new LoadablePlugin()
  ],
};

const server = {
  entry: ['./src/server/index.js'],
  name: 'server',
  target: 'node',
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [nodeExternals({ allowlist: ['webpack/hot/poll?1000'] })],
  module: {
    rules: [
      { test: /\.js?$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.(jpeg|png)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            },
          },
        ],
      }
    ],
  },
  plugins: [
    new webpack.ProgressPlugin()],
  output: { path: path.join(__dirname, '../build'), filename: 'server.js' },
};


const gameServer = {
  entry: ['./src/gameServer/index.js'],
  name: 'gameServer',
  target: 'node',
  externals: [nodeExternals({ allowlist: ['webpack/hot/poll?1000'] })],
  module: {
    rules: [
      { test: /\.js?$/, use: 'babel-loader', exclude: /node_modules/ },
      {
        test: /\.(jpeg|png)/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]'
            },
          },
        ],
      }
    ],
  },
  plugins: [
    new webpack.ProgressPlugin()],
  output: { path: path.join(__dirname, '../build-gameServer'), filename: 'gameServer.js' },
};

const game = {
  name: 'game',
  entry: {
    server: './src/gameServer/game/index.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(gif|png|jpe?g|svg|xml)$/i,
        use: 'file-loader'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      CANVAS_RENDERER: JSON.stringify(true),
      WEBGL_RENDERER: JSON.stringify(true)
    }),
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new CopyPlugin({
      patterns: [
        { from: 'src/assets', to: './assets' }
      ],
    }),
    new HtmlWebpackPlugin({
      title: 'serverView',
      template: './src/gameServer/game/server_index.html',
      publicPath: './',
      filename: 'server_index.html',
      chunks: ['server']
    })
  ],
  output: { path: path.join(__dirname, '../build-gameServer/public/game'), filename: 'game.[name].bundle.js' },
};

module.exports = [client, server, game, gameServer];
