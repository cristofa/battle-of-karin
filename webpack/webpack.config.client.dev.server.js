const webpack = require('webpack');

const config = require('./webpack.config.base');
const findConfig = require('./utils');

let client = findConfig(config, 'client');

client = {
  ...client,
  mode: 'development',
  output: {
    publicPath: '/',
  },
  plugins: [
    ...client.plugins,
    new webpack.ProvidePlugin({
      process: 'process/browser',
    })
  ],
  devServer: {
    host: '0.0.0.0',
    port: 3001,
    historyApiFallback: true,
    hot: true,
    static: ['./public', './build-client']
  },
};

module.exports = client;
