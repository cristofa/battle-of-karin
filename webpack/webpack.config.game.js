const path = require('path');
const config = require('./webpack.config.base');
const findConfig = require('./utils');

let game = findConfig(config, 'game');

game = {
  ...game,
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [
    ...game.plugins
  ],
  output: {
    path: path.join(__dirname, '../build-game'),
    publicPath: './'
  },
};

module.exports = game;
