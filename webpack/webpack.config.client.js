const webpack = require('webpack');

const path = require('path');
const config = require('./webpack.config.base');
const findConfig = require('./utils');

let client = findConfig(config, 'client');

client = {
  ...client,
  devtool: 'inline-source-map',
  plugins: [
    ...client.plugins,
    new webpack.ProvidePlugin({
      process: 'process/browser',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  output: {
    path: path.join(__dirname, '../build-client/'),
    publicPath: '/',
    filename: 'game.js',
  },
};

module.exports = client;
