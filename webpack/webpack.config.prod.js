const webpack = require('webpack');
const dotenv = require('dotenv');

const path = require('path');
const fs = require('fs');
const config = require('./webpack.config.base');
const findConfig = require('./utils');

let configPath;

const localPath = path.join(__dirname, '../dotenv');

if (fs.existsSync(localPath)) {
  console.log('Using ./dotenv config file.');
  configPath = './dotenv';
} else {
  console.log('Using ../karin-config/dotenv config file.');
  configPath = '../karin-config/dotenv';
}

const env = dotenv.config({ path: configPath}).parsed;
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next]);
  return prev;
}, {});


const client = findConfig(config, 'client');
client.mode = 'production';
client.output = {
  path: path.join(__dirname, '../build/public'),
  publicPath: '/',
  filename: 'game.js',
};
client.plugins.push(new webpack.DefinePlugin(envKeys));

const server = findConfig(config, 'server');
server.mode = 'production';
server.plugins.push(new webpack.DefinePlugin(envKeys));

const gameServer = findConfig(config, 'gameServer');
gameServer.mode = 'production';
gameServer.plugins.push(new webpack.DefinePlugin(envKeys));

const game = findConfig(config, 'game');
game.devtool = false;
game.mode = 'production';
game.output = {
  path: path.join(__dirname, '../build-gameServer/game'),
  filename: 'game.[name].min.js'
};

module.exports = [client, server, gameServer, game];
