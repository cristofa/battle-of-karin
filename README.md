### Battle of Karin

Karin is the name of an asteroid and asteroid family in the main asteroid belt.

Battle of Karin is a multiplayer asteroids game.
It uses:
* Phaser.js as a game engine
* React/Redux for the UI/HUD
* socket.io for communication
* node.js for the backend

### Development

To run the game locally you will need postgres DB. Check the `src/server/config.js` file for the details.
When running the game for the first time change following like in the `src/server/index.js` file:
```
await dbConnection.sync({force: false});
```

Change the argument to `{force: true}` to init the database.

Running `npm run start` will start the frontend and backend in development modes.
Frontend changes are reloaded automatically. If you change the game's logic on the backend
you can access http://127.0.0.1:3200/game/reload in order to reload the backend.

#### Discord
To enable discord login you will need to create a new discord app in the [Discord
Developer Portal](https://discord.com/developers/applications).

You will get a ClientID and a Secret. Create environment variables with those values,
for example by adding this to `~/.barshrc`:

```
export DISCORD_CLIENT_ID=8333333334
export DISCORD_SECRET=Gdadfasdf-dfasfdas-adfasfd-fdasfas
```

#### DB unit test
Separate DB is used to run some unit tests by:
> npm run testdb

Check the `src/server/config.js` file for the configuration details.

###Deploying

Creating production build requires additional config file with following variables:

```
DISCORD_CLIENT_ID=
DISCORD_SECRET=
#DB
DB_HOST=
DB_NAME=
DB_USER=
DB_PASS=

#Main server
HOST=
PORT=
SESSION_SECRET=
BASIC_AUTH_USER=
BASIC_AUTH_PASS=

#Game server
WS_PORT=
API_PORT=
GAME_API_USER=
GAME_API_PASS=

#game servers
GAME_HOST_EU=
GAME_HOST_US=
```

Check the webpack configuration to see how it's used. By default, it will look for this configuration in `../karin-config/dotenv` file.

### Docker
Running `npm run build` will generate two Docker containers:
* website - this is the website that serves frontend. It contains the main page, the frontend of the game, database and an API for storing results of the matches
* game-server - this is the backend of the game. You can deploy multiple game servers and add them to the list of available regions. All game-servers will send the results to the main server where it's stored in the DB

